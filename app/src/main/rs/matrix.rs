#pragma version(1)
#pragma rs java_package_name(edu.wisc.cs.renderscriptanalyzer)


rs_allocation matrixOther;

// Get a 2D location from a 1D matrix representation
static int getDataAt(int x, int y, int xDim, int yDim, rs_allocation data){
    return rsGetElementAt_int(data, x * yDim + y);
}

// get the current row of a 2D matrix based on a 1D index
static int getCurrRow(int x, int xDim, int yDim){
    return x / xDim;
}

// get the current column of a 2D matrix based on a 1D index
static int getCurrCol(int x, int xDim, int yDim){
    return x % xDim;
}

// adds two matrices
int __attribute__((kernel)) matrixAdd(int32_t in, uint32_t x){
    //use rsGetElementAt_int() to get our other elements
    return (in + rsGetElementAt_int(matrixOther, x));
}

// Dimensions for our matrices. yDim1 must equal xDim2
uint32_t xDim1;
uint32_t xDim2;
uint32_t yDim1;
uint32_t yDim2;
rs_allocation matrix1;
rs_allocation matrix2;

// multiplies two arbitrary matrices. Called for each result location
int __attribute__((kernel)) matrixMultiply(int32_t in, uint32_t x){
    int l = 0;
    int returnVal = 0;
    // find j and k by using x. Remember the result matrix will be xDim1 by yDim2
    int j = getCurrRow(x, xDim1, yDim2);
    int k = getCurrCol(x, xDim1, yDim2);
    for (l = 0; l < yDim1; l = l + 1){
        returnVal += getDataAt(j, l, xDim1, yDim1, matrix1) * getDataAt(l, k, xDim1, yDim1, matrix2);
    }
    return returnVal;
}

