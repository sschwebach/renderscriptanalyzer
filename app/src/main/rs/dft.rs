#pragma version(1)
#pragma rs java_package_name(edu.wisc.cs.renderscriptanalyzer)
#pragma rs_fp_full

rs_allocation data;

int m;

/*
Computes the dft for an array of size m. This array is a 2D double array, where the first element
represents the real part of the vector and the second element represents the imaginary part.
*/
double2 __attribute__((kernel)) dft(double2 in, uint32_t x){
    double2 result;
    result.x = 0.0;
    result.y = 0.0;
    double arg = -2.0 * 3.141592654 * (double) x / (double) m;

    int k = 0;
    for (k = 0; k < m; k = k + 1){
        float cosval = k * (float) arg;
        double cosarg = cos(cosval);
        double sinarg = sin(cosval);
        result.x += (rsGetElementAt_double2(data, k).x * cosarg - rsGetElementAt_double2(data, k).y * sinarg);
        result.y += (rsGetElementAt_double2(data, k).x * sinarg + rsGetElementAt_double2(data, k).y * cosarg);
    }
    return result / (double) m;
    //use rsGetElementAt_int() to get our other elements
}