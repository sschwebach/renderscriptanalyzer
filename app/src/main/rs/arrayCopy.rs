#pragma version(1)
#pragma rs java_package_name(edu.wisc.cs.renderscriptanalyzer)

// Simply returns the allocation's data to be sent to the new allocation
int __attribute__((kernel)) arrayCopy(int32_t in){
    return in;
}