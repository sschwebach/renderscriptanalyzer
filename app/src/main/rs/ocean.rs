#pragma version(1)
#pragma rs java_package_name(edu.wisc.cs.renderscriptanalyzer)


rs_allocation grid1;
rs_allocation grid2;
int xDim;
int yDim;
int odd;

int __attribute__((kernel)) ocean(int32_t in, uint32_t x){
    int aboveIndex;
    int belowIndex;
    int xLoc = x % xDim;
    int yLoc = x / xDim;
    // see if we have an edge case, if so just return the original number
    if (xLoc == 0 || xLoc == yDim - 1 || yLoc == 0 || yLoc == xDim -1 ){
        return in;
    }
    // otherwise do the rest of the work
    aboveIndex = x - xDim;
    belowIndex = x + xDim;
    //use rsGetElementAt_int() to get our other elements
    if (odd){
        return (in + rsGetElementAt_int(grid2, x - 1) + rsGetElementAt_int(grid2, x + 1) +
            rsGetElementAt_int(grid2, aboveIndex) + rsGetElementAt_int(grid2, belowIndex)) / 5;
    }
    return (in + rsGetElementAt_int(grid1, x - 1) + rsGetElementAt_int(grid1, x + 1) +
        rsGetElementAt_int(grid1, aboveIndex) + rsGetElementAt_int(grid1, belowIndex)) / 5;

}

void init(){

}