#pragma version(1)
#pragma rs java_package_name(edu.wisc.cs.renderscriptanalyzer)


rs_allocation grid1;
rs_allocation grid2;
int xDim;
int yDim;
int odd;

// Despite being a kernel, it will run a single time
int __attribute__((kernel)) oceanSerial(int32_t in, uint32_t x){
    //rsDebug("Grid is %d by %d", xDim, yDim);
    // time to be an idiot with renderscript
    int i = 0;
    for (i = 0; i < xDim * yDim; i = i + 1){
        //rsDebug("Iteration %d", i);
        // get the current element
        int data;
        if (odd) {
            data = rsGetElementAt_int(grid2, i);
        } else {
            data = rsGetElementAt_int(grid1, i);
        }

        int aboveIndex;
        int belowIndex;
        int xLoc = x % xDim;
        int yLoc = x / xDim;
        // see if we have an edge case, if so just return the original number
        if (xLoc == 0 || xLoc == yDim - 1 || yLoc == 0 || yLoc == xDim -1 ){
            // write data but don't return yet
            if (odd){
                rsSetElementAt_int(grid1, data, i);
            } else {
                rsSetElementAt_int(grid2, data, i);
            }
            continue;
        }
        // otherwise do the rest of the work
        aboveIndex = x - xDim;
        belowIndex = x + xDim;
        //use rsGetElementAt_int() to get our other elements
        if (odd){
            rsSetElementAt_int(grid1, (data + rsGetElementAt_int(grid2, x - 1) + rsGetElementAt_int(grid2, x + 1) +
                rsGetElementAt_int(grid2, aboveIndex) + rsGetElementAt_int(grid2, belowIndex)) / 5, i);
        } else {
            rsSetElementAt_int(grid2, (data + rsGetElementAt_int(grid1, x - 1) + rsGetElementAt_int(grid1, x + 1) +
                rsGetElementAt_int(grid1, aboveIndex) + rsGetElementAt_int(grid1, belowIndex)) / 5, i);
        }

    }
    rsSendToClient(1, &grid1, sizeof(int) * xDim * yDim);
    rsSendToClient(2, &grid2, sizeof(int) * xDim * yDim);
    // done
    return 1;
}
