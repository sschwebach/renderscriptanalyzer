#pragma version(1)
#pragma rs java_package_name(edu.wisc.cs.renderscriptanalyzer)

static uint32_t r0 = 0x6635e5ce, r1 = 0x13bf026f, r2 = 0x43225b59, r3 = 0x3b0314d0;
int maxBranches;
int arraySize;
rs_allocation sourceArray;



static int rand(){
    // Generate a random number between 0-1
    uint32_t t = r0 ^ (r0 << 11);
    r0 = r1; r1 = r2; r2 = r3;
    r3 = r3 ^ (r3 >> 19) ^ t ^ (t >> 8);
    int result = (int) r3;
    rsDebug("Rand is ", result);
    if (result < 0){
        return -result;
    }
    return result;
}

// returns a random integer based on the path chosen
// the integer is completely arbitrary to confuse the compiler
static int choosePath(int maxBranches){
    int path = rand() % maxBranches;
    switch(path){
        case 0:
            return 0;
            break;
        case 1:
            return 123;
            break;
        case 2:
            return 154;
            break;
        case 3:
            return 103;
            break;
        case 4:
            return 513;
            break;
        case 5:
            return 65189;
            break;
        case 6:
            return -1981;
            break;
        case 7:
            return 5569151;
            break;
        case 8:
            return -661218;
            break;
        case 9:
            return 251;
            break;
        case 10:
            return 21684;
            break;
        case 11:
            return -9991;
            break;
        case 12:
            return 565498;
            break;
        case 13:
            return 1356914;
            break;
        case 14:
            return 64/41;
            break;
        case 15:
            return -15614999;
            break;
        case 16:
            return 54110;
            break;
        case 17:
            return 5;
            break;
        case 18:
            return 9581652;
            break;
        case 19:
            return 98412211178;
            break;
    }
    return -1;
}

int __attribute__((kernel)) branchDiverge(int32_t in){
    return choosePath(maxBranches);
}

int __attribute__((kernel)) memDiverge(int32_t in){
    int location = rand() % arraySize;
    rsDebug("Element at ", location);
    return rsGetElementAt_int(sourceArray, location);
}