package edu.wisc.cs.renderscriptanalyzer.Widgets;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkManager;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EArgs;

/**
 * Created by Sam on 3/15/2016.
 * This view represents a view that will be added for every custom parameter in a benchmark.
 * It will name the parameter and allow the user to input a value.
 * The input form will vary based on the type of parameter. For example, a boolean parameter will
 * have a checkmark, while a string parameter will have an edittext with the standard keyboard
 * (an int/long/double parameter will have en edittext with the number keyboard).
 */
public class ArgumentView extends LinearLayout {
    private Context mContext;
    // TextView to represent the argument name
    private TextView mParamNameView;
    // Possible forms of data input
    private CheckBox mBooleanCheck;
    private EditText mTextInput;
    // Name of the benchmark
    private String mBenchmarkName;
    // Name of the argument
    private String mArgumentName;
    // Type of parameter
    private EArgs.ParamType mType;
    // The various values this can take
    private String mStringVal;
    private int mIntVal;
    private boolean mBoolVal;
    private double mDoubleVal;
    private long mLongVal;

    public ArgumentView(Context context, String benchmarkName, String argName, EArgs.ParamType argType, Object defaultVal) {
        super(context);
        setup(context, benchmarkName, argName, argType, defaultVal);
    }

    public ArgumentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ArgumentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void setup(Context context, String benchmarkName, String argName, EArgs.ParamType argType, Object defaultVal) {
        this.setPadding(10, 10, 10, 10);
        this.setOrientation(LinearLayout.VERTICAL);
        mContext = context;
        mArgumentName = argName;
        mBenchmarkName = benchmarkName;
        mType = argType;
        // Add the textview for the name regardless
        mParamNameView = new TextView(context);
        mParamNameView.setText(argName);
        mParamNameView.setId(View.generateViewId());
        // Create layout parameters for our text view
        LayoutParams textParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        textParams.setMarginStart(10);
        textParams.setMarginEnd(10);
        mParamNameView.setLayoutParams(textParams);
        this.addView(mParamNameView);
        LayoutParams valueParams;
        // Create layout parameters for the other view we don't know about yet
        if (mType == EArgs.ParamType.BOOLEAN) {
            valueParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        } else {
            valueParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        }
        valueParams.setMarginStart(10);
        valueParams.setMarginEnd(10);
        switch (mType) {
            case INT:
                mIntVal = (int) defaultVal;
                // Make a text view for our integer
                mTextInput = new EditText(context);
                mTextInput.setHint("" + mIntVal);
                mTextInput.setSingleLine();
                mTextInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
                // set the layout params
                mTextInput.setLayoutParams(valueParams);
                // add the view to the layout
                this.addView(mTextInput);
                break;
            case STRING:
                mStringVal = (String) defaultVal;
                // Make a text view for our string
                mTextInput = new EditText(context);
                mTextInput.setHint("" + mStringVal);
                mTextInput.setSingleLine();
                // set the layout params
                mTextInput.setLayoutParams(valueParams);
                // add the view to the layout
                this.addView(mTextInput);
                break;
            case BOOLEAN:
                mBoolVal = (Boolean) defaultVal;
                // Make a checkbox for our boolean
                mBooleanCheck = new CheckBox(context);
                mBooleanCheck.setChecked(mBoolVal);
                // set the layout params
                mBooleanCheck.setLayoutParams(valueParams);
                this.addView(mBooleanCheck);
                break;
            case DOUBLE:
                mDoubleVal = (double) defaultVal;
                // Make a text view for our double
                mTextInput = new EditText(context);
                mTextInput.setHint("" + mDoubleVal);
                mTextInput.setSingleLine();
                mTextInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                // set the layout params
                mTextInput.setLayoutParams(valueParams);
                // add the view to the layout
                this.addView(mTextInput);
                break;
            case LONG:
                mLongVal = (long) defaultVal;
                // Make a text view for our string
                mTextInput = new EditText(context);
                mTextInput.setHint("" + mLongVal);
                mTextInput.setSingleLine();
                mTextInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
                // set the layout params
                mTextInput.setLayoutParams(valueParams);
                // add the view to the layout
                this.addView(mTextInput);
                break;
        }
        this.invalidate();
    }

    /**
     * Adds this view's argument and values to the supplied EArgs.
     *
     * @param args The EArgs to which to add our argument
     */
    public void addArgument(EArgs args) throws IllegalArgumentException {
        switch (mType) {
            case INT:
                if (mTextInput.getText().toString().isEmpty()){
                    args.addArgument(this.mArgumentName, mIntVal);
                    break;
                }
                try {
                    mIntVal = Integer.parseInt(mTextInput.getText().toString());
                    BenchmarkManager.getInstance().find(mBenchmarkName).validateArgument(mArgumentName, mIntVal);
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException("Value for parameter " + this.mArgumentName + " is invalid!");
                }
                args.addArgument(this.mArgumentName, mIntVal);
                break;
            case STRING:
                if (mTextInput.getText().toString().isEmpty()){
                    args.addArgument(this.mArgumentName, mStringVal);
                    break;
                }
                mStringVal = mTextInput.getText().toString();
                BenchmarkManager.getInstance().find(mBenchmarkName).validateArgument(mArgumentName, mStringVal);
                args.addArgument(this.mArgumentName, mStringVal);
                break;
            case BOOLEAN:
                mBoolVal = mBooleanCheck.isChecked();
                args.addArgument(this.mArgumentName, mBoolVal);
                break;
            case DOUBLE:
                if (mTextInput.getText().toString().isEmpty()){
                    args.addArgument(this.mArgumentName, mDoubleVal);
                    break;
                }
                try {
                    mDoubleVal = Double.parseDouble(mTextInput.getText().toString());
                    BenchmarkManager.getInstance().find(mBenchmarkName).validateArgument(mArgumentName, mDoubleVal);
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException("Value for parameter " + this.mArgumentName + " is invalid!");
                }
                args.addArgument(this.mArgumentName, mDoubleVal);
                break;
            case LONG:
                if (mTextInput.getText().toString().isEmpty()){
                    args.addArgument(this.mArgumentName, mLongVal);
                    break;
                }
                try {
                    mLongVal = Long.parseLong(mTextInput.getText().toString());
                    BenchmarkManager.getInstance().find(mBenchmarkName).validateArgument(mArgumentName, mLongVal);
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException("Value for parameter " + this.mArgumentName + " is invalid!");
                }
                args.addArgument(this.mArgumentName, mLongVal);
                break;
        }
    }

}
