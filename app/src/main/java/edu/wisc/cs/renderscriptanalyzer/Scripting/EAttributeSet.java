package edu.wisc.cs.renderscriptanalyzer.Scripting;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.ExecutionMode;

/**
 * Created by Sam on 3/10/2016.
 * Stands for Execution Attribute Set. Represents all attributes of single execution configuration
 * (the configuration might be executed more than once).
 * Example for ocean could be the grid size and number of iterations for a single ocean execution,
 * as well as the number of iterations for that setup.
 */
public class EAttributeSet {
    // Command line mArguments for the actual program
    private EArgs mArgs;
    // Name of the benchmark to execute
    private String mBenchmarkName;
    // The number of iterations to execute this configuration
    private int mIterations;
    // An array to hold the execution times once we're done
    private long[] mExecutionTimes;
    // Specify which mode we want to execute
    private ExecutionMode mMode;

    /**
     * Default constructor. Edit as needed.
     */
    public EAttributeSet() {

    }

    /**
     * Gets the mArguments for this execution.
     * @return The mArguments for the execution
     */
    public EArgs getArgs(){
        return mArgs;
    }

    /**
     * Gets the name for this execution
     * @return
     */
    public String getBenchmarkName(){
        return mBenchmarkName;
    }

    /**
     * Get the number of iterations for this execution
     * @return
     */
    public int getIterations(){
        return mIterations;
    }

    /**
     * Get the mode of execution (currently either serial or CUDA)
     * @return
     */
    public ExecutionMode getExecutionMode(){
        return mMode;
    }

    /**
     * Constructor for making an attribute set from the script generator.
     * There is no need to allocate the execution times array since we will be storing this
     * as a script.
     *
     * @param args          Program mArguments created from list.
     * @param benchmarkName Benchmark tag.
     * @param iterations    Number of iterations.
     * @param mode          Mode (currently either serial or CUDA)
     */
    public EAttributeSet(EArgs args, String benchmarkName, int iterations, ExecutionMode mode) {
        this.mArgs = args;
        this.mBenchmarkName = benchmarkName;
        this.mIterations = iterations;
        this.mMode = mode;
    }

    /**
     * Add a value to the execution times array. If index is out of the array's size nothing is
     * added.
     *
     * @param executionTime time to add
     * @param index         index of the array into which to put the value
     */
    public void addExecutionTime(long executionTime, int index) {
        if (index >= mExecutionTimes.length || index < 0) {
            // TODO error message? Honestly this shouldn't happen
            return;
        }
        mExecutionTimes[index] = executionTime;
    }

    /**
     * Returns a JSONObject of this attribute set, along with any execution times that have been
     * collected. Usually called when taking the results and saving them off somewhere...
     *
     * @return A JSONObject representing the configuration and any results that were collected.
     */
    public JSONObject toJSON() {
        JSONObject toReturn = new JSONObject();
        try {
            toReturn.put(AttributeConstants.scriptAttr_benchmarkArgs, mArgs.getJSONObject());
            toReturn.put(AttributeConstants.scriptAttr_numIterations, mIterations);
            toReturn.put(AttributeConstants.scriptAttr_benchmarkName, mBenchmarkName);
            if (mExecutionTimes != null) {
                toReturn.put(AttributeConstants.scriptAttr_executionResultTimes, new JSONArray(mExecutionTimes));
            }
            toReturn.put(AttributeConstants.scriptAttr_exeMode, mMode);
        } catch (JSONException e) {
            // TODO do something on error?
        }
        return toReturn;
    }

    /**
     * Takes in a script file and parses out a list of all execution attribute sets.
     * Each different execution setup represents a specific setup of a benchmark.
     * These setups can be for different benchmarks in a single file, as well as be executed
     * multiple times.
     * <p/>
     * This is used when taking in a script file and executing it
     *
     * @param script
     * @return
     */
    public static List<EAttributeSet> parseScriptFile(File script) {
        List<EAttributeSet> toReturn = new ArrayList<EAttributeSet>();
        // Open the script file
        // Each line will represent a separate configuration, containing the name of the program
        // to be executed, the mArguments to the program, and the number of times to execute it.
        // Each of these lines will be parsed and made into a single EAttributeSet object.
        // Additionally, an array will be allocated to hold all the execution times
        try {
            Scanner fileIn = new Scanner(script);
            String currLine;
            while (fileIn.hasNextLine()) {
                // Get the next line
                currLine = fileIn.nextLine();
                // Make it into a json object
                JSONObject lineJSON = new JSONObject(currLine);
                EAttributeSet toAdd = new EAttributeSet();
                toAdd.mArgs = new EArgs(lineJSON.getJSONArray(AttributeConstants.scriptAttr_benchmarkArgs));
                toAdd.mBenchmarkName = lineJSON.getString(AttributeConstants.scriptAttr_benchmarkName);
                toAdd.mIterations = lineJSON.getInt(AttributeConstants.scriptAttr_numIterations);
                toAdd.mExecutionTimes = new long[toAdd.mIterations];
                toAdd.mMode = ExecutionMode.valueOf(lineJSON.get(AttributeConstants.scriptAttr_exeMode).toString());
                toReturn.add(toAdd);
            }
            fileIn.close();
        } catch (FileNotFoundException e) {
            // TODO do something here?
        } catch (JSONException e) {
            // TODO do something here?
        }

        return toReturn;
    }

}
