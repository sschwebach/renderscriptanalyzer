package edu.wisc.cs.renderscriptanalyzer.Benchmarking;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import edu.wisc.cs.renderscriptanalyzer.Scripting.BenchmarkFile;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EAttributeSet;

/**
 * Created by Sam on 3/21/2016.
 */
public class FileManager {
    public static final String SCRIPT_FILE_EXTENSION = "bes";
    public static final String RESULT_FILE_EXTENSION = "result";
    private static FileManager ourInstance = new FileManager();

    public static FileManager getInstance() {
        return ourInstance;
    }

    private FileManager() {
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    /**
     * Gets a list of all benchmark files that exist in our documents directory.
     * @return
     */
    public List<BenchmarkFile> getBenchmarks(){
        List<BenchmarkFile> toReturn = new ArrayList<BenchmarkFile>();
        File path = getDefaultFilepath();
        File file[] = path.listFiles();
        for (int i=0; i < file.length; i++)
        {
            String extension = getFileExtension(file[i]);
            if (getFileExtension(file[i]).equalsIgnoreCase(SCRIPT_FILE_EXTENSION)){
                Log.e("Files", "Found file " + file[i].getName());
                // TODO parse out the file and get everything
                BenchmarkFile toAdd = new BenchmarkFile(EAttributeSet.parseScriptFile(file[i]), file[i].getName());
                toReturn.add(toAdd);
            }

        }
        return toReturn;
    }

    /**
     * Returns the default filepath this application uses
     * @return
     */
    public File getDefaultFilepath(){
        File documentPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        // attempt to make a directory
        documentPath = new File(documentPath.getAbsolutePath() + "/Benchmarking");
        documentPath.mkdirs();
        return documentPath;
    }

    /**
     * Returns the path for where files should be uploaded for copying into the application folder.
     * @return A file specifying the storage directory
     */
    public File getStoredEXEPath(){
        return new File("/data/local/tmp");
    }

    /**
     * Gets the filepath for where any executables will be executed, as the application doesn't
     * have execute permissions in every folder.
     * @return The file for the application's directory
     */
    public File getExecutableFilepath(){
        return new File("/data/data/edu.wisc.cs.renderscriptanalyzer");
    }

    /**
     * Gets the file extension of a given file, defined as the string following the last dot
     * @param file The file from which to get the extension
     * @return The file extension, without the '.' character
     */
    private String getFileExtension(File file) {
        String name = file.getName();
        try {
            return name.substring(name.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }
    }



    /**
     * Creates a new file in the documents directory for use by our app.
     * @param fileName The filename of the new file.
     * @return A file handle to the created file.
     */
    public File getScriptStorageDir(String fileName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(getDefaultFilepath(), fileName);
        if (!file.mkdirs()) {
            Log.e("File Creation", "Directory not created");
        }
        return file;
    }

    /**
     * Write data to a file with the specified filename.
     * @param fileName The name of the file, plus extension
     * @param stringData The string data to write to the file
     */
    public void writeFile(String fileName, String stringData, boolean isScript) {
        // Create a path where we will place our picture in the user's
        // public pictures directory.  Note that you should be careful about
        // what you place here, since the user often manages these files.  For
        // pictures and other media owned by the application, consider
        // Context.getExternalMediaDir().
        File path = getDefaultFilepath();
        File file;
        if (isScript) {
            file = new File(path, fileName + "." + SCRIPT_FILE_EXTENSION);
        } else{
            file = new File(path, fileName + "." + RESULT_FILE_EXTENSION);
        }
        if (file.exists() && !file.isDirectory()){
            // attempt to append an integer until it doesn't exist
            int append = 1;
            while (file.exists() && !file.isDirectory()){
                if (isScript) {
                    file = new File(path, fileName + "(" + append + ")." + SCRIPT_FILE_EXTENSION);
                } else{
                    file = new File(path, fileName + "(" + append + ")." + RESULT_FILE_EXTENSION);
                }
                append++;
            }
        }

        try {
            // Make sure the Pictures directory exists.
            path.mkdirs();

            // Very simple code to copy a picture from the application's
            // resource into the external file.  Note that this code does
            // no error checking, and assumes the picture is small (does not
            // try to copy it in chunks).  Note that if external storage is
            // not currently mounted this will silently fail.
            OutputStream os = new FileOutputStream(file);
            byte[] data = stringData.getBytes();
            os.write(data);
            Log.e("File storage", "Made file " + file.getAbsolutePath());
            os.close();
        } catch (IOException e) {
            // Unable to create file, likely because external storage is
            // not currently mounted.
            Log.e("ExternalStorage", "Error writing " + file, e);
        }
    }

}
