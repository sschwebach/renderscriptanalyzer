package edu.wisc.cs.renderscriptanalyzer.Widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 * This view represents a textview within a scrollview that is meant to function like a terminal
 * window. It is intended to be instantiated in an XML layout, such as a dialog.
 * Created by samsc on 3/28/2016.
 */
public class TerminalView extends ScrollView{
    private TextView mTextPrintout;

    public TerminalView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mTextPrintout = new TextView(context);
        LayoutParams textParams = new ScrollView.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        this.addView(mTextPrintout, textParams);
        scrollToBottom();
    }

    /**
     * Prints the supplied string, followed by a newline.
     * @param line The string to print.
     */
    public void println(String line){
        try {
            mTextPrintout.append(line + "\n");
            this.invalidate();
            scrollToBottom();
        } catch (OutOfMemoryError e){
            clearText();
            println(line);
        }
    }

    /**
     * Prints the supplied string, NOT followed by a newline.
     * @param text The string to print.
     */
    public void print(String text){
        try {
            mTextPrintout.append(text);
            this.invalidate();
            scrollToBottom();
        } catch (OutOfMemoryError e){
            clearText();
            print(text);
        }
    }

    /**
     * Clears all text from our view.
     */
    public void clearText(){
        mTextPrintout.setText("");
        this.invalidate();
        scrollToBottom();
    }

    private void scrollToBottom(){
        this.post(new Runnable() {
            @Override
            public void run() {
                TerminalView.this.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    public boolean isInEditMode(){
        return super.isInEditMode();
    }
}
