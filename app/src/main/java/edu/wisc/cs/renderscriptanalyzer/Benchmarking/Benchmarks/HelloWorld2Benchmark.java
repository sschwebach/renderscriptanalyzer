package edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkExecution;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EArgs;

/**
 * Created by Sam on 4/3/2016.
 * A second test benchmark to test the application's ability to distinguish benchmarks. Will not
 * be used in any meaningful way.
 */
public class HelloWorld2Benchmark extends BenchmarkExecution {

    @Override
    protected void doIterationSetup(EArgs args) {
        println("Setting up for RenderScript execution iteration");
    }

    @Override
    protected void doIterationWork(EArgs args) {
        println("Hello2, RenderScript world!");
    }

    @Override
    protected void doIterationCleanup(EArgs args) {
        println("Cleaning up from RenderScript execution iteration");
    }

    @Override
    protected int getLayoutRes() {
        return 0;
    }

    @Override
    protected void doTotalSetup(EArgs args) {
        println("Benchmark Begin!");
    }

    @Override
    protected void doTotalCompletion(EArgs args) {
        println("Work done");
    }

    @Override
    protected void addProgramArguments(EArgs args) {
        args.addArgument("Extra message 2", "Default extra message 2!");
        args.addArgument("Extra int 2", 0);
        args.addArgument("Extra long 2", 0L);
        args.addArgument("Extra double 2", 0.0);
        args.addArgument("Extra boolean 2", false);
        mBenchmarkName = "Hello World 2 Benchmark";
    }

    @Override
    public boolean isValidArgument(String argName, Object value){
        return true;
    }

    @Override
    protected void getProgramArgs(EArgs args) {

    }
}
