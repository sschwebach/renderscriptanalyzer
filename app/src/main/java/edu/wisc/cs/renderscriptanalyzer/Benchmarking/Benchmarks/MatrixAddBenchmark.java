package edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks;

import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.Script;
import android.renderscript.Type;

import java.util.Random;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkExecution;
import edu.wisc.cs.renderscriptanalyzer.ScriptC_matrix;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EArgs;

/**
 * Created by Sam on 4/26/2016.
 * A benchmark to add two matrices together, a very simple operation.
 */
public class MatrixAddBenchmark extends BenchmarkExecution {
    private static final String MADD_DIMENSION = "Matrix Dimension";
    private static final String MADD_TIMESTEPS = "Timesteps";
    private static final String MADD_DEBUG = "Debug Mode";
    private int[] mBeginGrid;
    private int[] mResultGrid;
    private Allocation mBeginAlloc;
    private Allocation mResultAlloc;
    private int mDimension;
    private int mTimeSteps;
    private boolean mDebug;
    private ScriptC_matrix mMatrixScript;

    @Override
    protected void doIterationSetup(EArgs args) {

    }

    @Override
    protected void doIterationWork(EArgs args) {
        // make the script
        mMatrixScript = new ScriptC_matrix(mRenderScript);
        // allocate space for the arrays
        mBeginGrid = new int[mDimension * mDimension];
        mResultGrid = new int[mDimension * mDimension];
        // initialize both arrays
        Random rng = new Random();
        for (int i = 0; i < mDimension * mDimension; i++) {
            if (mDebug) {
                if (i / mDimension == 0 || i % mDimension == 0 || i / mDimension == mDimension - 1 || i % mDimension == mDimension - 1) {
                    mBeginGrid[i] = 100;
                    mResultGrid[i] = 100;
                } else {
                    mBeginGrid[i] = 50;
                    mResultGrid[i] = 50;
                }
            } else {
                mBeginGrid[i] = rng.nextInt() % 1000000;
                mResultGrid[i] = rng.nextInt() % 1000000;
            }
        }
        // create our allocations
        mBeginAlloc = Allocation.createTyped(mRenderScript, Type.createX(mRenderScript, Element.I32(mRenderScript), mDimension * mDimension));
        mResultAlloc = Allocation.createTyped(mRenderScript, Type.createX(mRenderScript, Element.I32(mRenderScript), mDimension * mDimension));
        // copy our begin matrix to our allocation
        mBeginAlloc.copy1DRangeFrom(0, mDimension * mDimension, mBeginGrid);
        mResultAlloc.copy1DRangeFrom(0, mDimension * mDimension, mResultGrid);
        // set globals within the script
        mMatrixScript.set_matrixOther(mBeginAlloc);
        // begin the script
        Script.LaunchOptions lOptions = new Script.LaunchOptions();
        for (int i = 0; i < mTimeSteps; i++) {
            mMatrixScript.forEach_matrixAdd(mResultAlloc, mResultAlloc, lOptions);
        }

        // copy the data back
        mResultAlloc.copyTo(mResultGrid);
    }

    @Override
    protected void doIterationCleanup(EArgs args) {
        if (mDebug) {
            printMatrix(mDimension, mDimension, mBeginGrid);
            println("Plus (" + mTimeSteps + " times)");
            printMatrix(mDimension, mDimension, mBeginGrid);
            println("Equals");
            printMatrix(mDimension, mDimension, mResultGrid);
        }
        mResultAlloc.destroy();
        mBeginAlloc.destroy();
    }

    private void printMatrix(int xDim, int yDim, int[] matrix) {
        print("[");
        for (int i = 0; i < xDim; i++) {
            for (int j = 0; j < yDim; j++) {
                print(matrix[i * yDim + j] + ", ");
            }
            println("");
        }
        print("[");
    }

    @Override
    protected int getLayoutRes() {
        return 0;
    }

    @Override
    protected void doTotalSetup(EArgs args) {

    }

    @Override
    protected void doTotalCompletion(EArgs args) {

    }

    @Override
    protected void addProgramArguments(EArgs args) {
        mBenchmarkName = "Matrix Add Benchmark";
        args.addArgument(MADD_DIMENSION, 10);
        args.addArgument(MADD_TIMESTEPS, 1);
        args.addArgument(MADD_DEBUG, false);
    }

    @Override
    protected void getProgramArgs(EArgs args) {
        mDimension = args.getInt(MADD_DIMENSION, 10);
        mTimeSteps = args.getInt(MADD_TIMESTEPS, 1);
        mDebug = args.getBoolean(MADD_DEBUG, false);
    }

}
