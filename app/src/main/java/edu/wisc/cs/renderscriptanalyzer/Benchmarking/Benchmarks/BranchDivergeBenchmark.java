package edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks;

import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.Script;
import android.renderscript.Type;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkExecution;
import edu.wisc.cs.renderscriptanalyzer.ScriptC_divergence;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EArgs;

/**
 * Created by Sam on 5/8/2016.
 */
public class BranchDivergeBenchmark extends BenchmarkExecution {
    private static String BRANCH_DIV_ITERATIONS = "Number of iterations";
    private static String BRANCH_DIV_MAXBRANCHES = "Maximum number of branches (<= 20)";
    private static String BRANCH_DIV_DEBUG = "Debug Mode";
    private int mIterations;
    private int mBranches;
    private boolean mDebug;
    private int[] mDestArray;
    private Allocation mDestAlloc;
    private ScriptC_divergence mDiverge;

    @Override
    protected void doIterationSetup(EArgs args) {

    }

    @Override
    protected void doIterationWork(EArgs args) {
        // allocate the array, no need to initialize
        mDestArray = new int[mIterations];
        // allocate the allocation, no need to copy data over
        mDestAlloc = Allocation.createTyped(mRenderScript, Type.createX(mRenderScript, Element.I32(mRenderScript), mIterations));
        // create the renderscript
        mDiverge = new ScriptC_divergence(mRenderScript);
        // set global
        mDiverge.set_maxBranches(mBranches);
        // start renderscript
        Script.LaunchOptions lOptions = new Script.LaunchOptions();
        mDiverge.forEach_branchDiverge(mDestAlloc, mDestAlloc, lOptions);
        // copy data back
        mDestAlloc.copyTo(mDestArray);
    }

    @Override
    protected void doIterationCleanup(EArgs args) {
        // if in debug mode, print the results
        if (mDebug){
            println("Result grid");
            print("[");
            for (int i = 0; i < mIterations; i++){
                print(mDestArray[i] + ", ");
            }
            println("]");
        }
        // destroy our allocations
        mDestAlloc.destroy();
    }

    @Override
    protected int getLayoutRes() {
        return 0;
    }

    @Override
    protected void doTotalSetup(EArgs args) {

    }

    @Override
    protected void doTotalCompletion(EArgs args) {

    }

    @Override
    protected void addProgramArguments(EArgs args) {
        mBenchmarkName = "Branch Divergence Benchmark";
        args.addArgument(BRANCH_DIV_ITERATIONS, 10);
        args.addArgument(BRANCH_DIV_MAXBRANCHES, 20);
        args.addArgument(BRANCH_DIV_DEBUG, false);
    }

    @Override
    protected void getProgramArgs(EArgs args) {
        mIterations = args.getInt(BRANCH_DIV_ITERATIONS, 10);
        mBranches = args.getInt(BRANCH_DIV_MAXBRANCHES, 20);
        mDebug = args.getBoolean(BRANCH_DIV_DEBUG, false);
    }

}
