package edu.wisc.cs.renderscriptanalyzer.Screens;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkExecution;

/**
 * Created by Sam on 3/10/2016.
 */
public interface BenchmarkActivityListener {
    void onTaskBegin(BenchmarkExecution f, int numIterations);
    void onTaskProgress(BenchmarkExecution f, final int iterationCompleted);
    void onTaskComplete(BenchmarkExecution f, String data);
    void onTaskInterrupted(BenchmarkExecution f, final int iterationsCompleted, String data);

}
