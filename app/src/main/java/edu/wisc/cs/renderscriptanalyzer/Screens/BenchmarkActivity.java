package edu.wisc.cs.renderscriptanalyzer.Screens;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkManager;
import edu.wisc.cs.renderscriptanalyzer.R;

public class BenchmarkActivity extends AppCompatActivity implements BenchmarkListFragment.BenchmarkListListener{
    // The current fragment we're using
    private BenchmarkActivityFragment mCurrFragment;
    private Button mActionButton;
    private FrameLayout mFragmentContainer;
    private Toolbar mToolbar;
    private BenchmarkListFragment mListFragment;
    private FloatingActionButton fab;
    private ActivityState mState;
    // Used to transition back to the correct state after execution
    private ActivityState mPrevState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BenchmarkManager.getInstance().init(this);
        mState = ActivityState.LIST_NONE_SEL;
        setContentView(R.layout.activity_benchmark);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("Benchmarks");
        setSupportActionBar(mToolbar);
        mFragmentContainer = (FrameLayout) findViewById(R.id.frame_content);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabClick(view);

            }
        });
        // connect to the benchmark manager
        BenchmarkManager.setActivityHandle(this);
        mListFragment = new BenchmarkListFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.frame_content, mListFragment).commit();
        mCurrFragment = mListFragment;
        mListFragment.attach(this);
        mListFragment.setBenchmarkListListener(this);
        mListFragment.onFragmentFocused();
    }

    @Override
    public void onBackPressed() {
        if (mState == ActivityState.EXECUTE){
            return;
        }
        if (mState == ActivityState.CREATE) {
            mState = ActivityState.LIST_NONE_SEL;
            invalidateOptionsMenu();
            mListFragment.clearListCheck();
            mListFragment.refreshList();
        }
        super.onBackPressed();
    }

    private void fabClick(View v) {
        if (mCurrFragment != null) {
            mCurrFragment.onActionButtonPressed();
        }
        FragmentTransaction ft = null;
        // Now possibly change states
        switch (mState) {
            case CREATE:
                transition(ActivityState.LIST_NONE_SEL);
                break;
            case EXECUTE:
                // Not sure what to do here yet
            case LIST_SEL_ONE:
            case LIST_SEL_TWO:
            case LIST_NONE_SEL:
                transition(ActivityState.CREATE);
                break;
        }
    }

    private void transition(ActivityState newState) {
        FragmentTransaction ft;
        switch (mState) {
            case CREATE:
                ft = getSupportFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.anim_fade_in, R.anim.anim_fade_out);
                ft.replace(R.id.frame_content, mListFragment).commit();
                mCurrFragment = mListFragment;
                mCurrFragment.onFragmentFocused();
                fab.show();
                mState = ActivityState.LIST_NONE_SEL;
                mListFragment.refreshList();
                //mToolbar.getMenu().findItem(R.id.action_cancel).setVisible(false);
                invalidateOptionsMenu();
                // Save our current creation if possible
                break;
            case LIST_SEL_ONE:
            case LIST_SEL_TWO:
            case LIST_NONE_SEL:
                // Bring up the creation fragment
                BenchmarkCreationFragment createFragment = new BenchmarkCreationFragment();
                ft = getSupportFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.anim_fade_in, R.anim.anim_fade_out);
                ft.addToBackStack(null);
                ft.replace(R.id.frame_content, createFragment).commit();
                mCurrFragment = createFragment;
                mCurrFragment.attach(this);
                mCurrFragment.onFragmentFocused();
                fab.hide();
                mState = ActivityState.CREATE;
                invalidateOptionsMenu();
                break;
        }
        invalidateOptionsMenu();
        mState = newState;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_benchmark, menu);
        switch (mState){
            case CREATE:
                menu.findItem(R.id.action_cancel).setVisible(true);
                menu.findItem(R.id.action_execute_one).setVisible(false);
                menu.findItem(R.id.action_execute_multiple).setVisible(false);
                break;
            case LIST_SEL_ONE:
                menu.findItem(R.id.action_cancel).setVisible(false);
                menu.findItem(R.id.action_execute_one).setVisible(true);
                menu.findItem(R.id.action_execute_multiple).setVisible(false);
                break;
            case LIST_SEL_TWO:
                menu.findItem(R.id.action_cancel).setVisible(false);
                menu.findItem(R.id.action_execute_one).setVisible(false);
                menu.findItem(R.id.action_execute_multiple).setVisible(true);
                break;
            case LIST_NONE_SEL:
                menu.findItem(R.id.action_cancel).setVisible(false);
                menu.findItem(R.id.action_execute_one).setVisible(false);
                menu.findItem(R.id.action_execute_multiple).setVisible(false);
                break;
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_cancel) {
            this.onBackPressed();
        }
        if (id == R.id.action_execute_multiple || id == R.id.action_execute_one){
            // TODO add code here
            mListFragment.startExecution();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Sets the button's enabled property, allowing others to do it.
     *
     * @param enabled True for the button to be enabled, false for disabled.
     */
    public void setButtonEnable(boolean enabled) {
        if (fab != null) {
            fab.setEnabled(enabled);
        }
    }

    /**
     * Sets the visibility of the floating action button.
     *
     * @param visible True for visible, false for invisible.
     */
    public void setButtonVisible(boolean visible) {
        if (visible) {
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.INVISIBLE);
        }
    }

    private void revealView(View view) {

        int cx = (view.getLeft() + view.getRight()) / 2;
        int cy = (view.getTop() + view.getBottom()) / 2;
        float radius = Math.max(view.getWidth(), view.getHeight()) * 2.0f;

        if (view.getVisibility() == View.INVISIBLE) {
            view.setVisibility(View.VISIBLE);
            Animator reveal = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, radius);
            reveal.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    BenchmarkActivity.this.findViewById(R.id.fab).setVisibility(View.INVISIBLE);
                }
            });
            reveal.start();
        } else {
            Animator reveal = ViewAnimationUtils.createCircularReveal(
                    view, cx, cy, radius, 0);
            reveal.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    BenchmarkActivity.this.findViewById(R.id.fab).setVisibility(View.INVISIBLE);
                }
            });
            reveal.start();
        }
    }

    public void setFabImage(final int resID) {
        Context c = this.getBaseContext();
        final Animation anim_out = AnimationUtils.loadAnimation(c, android.R.anim.fade_out);
        final Animation anim_in = AnimationUtils.loadAnimation(c, android.R.anim.fade_in);
        anim_out.setDuration(100);
        anim_in.setDuration(100);
        anim_out.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                fab.setImageDrawable(getResources().getDrawable(resID, null));
                anim_in.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }
                });
                fab.startAnimation(anim_in);
            }
        });
        fab.startAnimation(anim_out);
    }

    @Override
    public void onFirstItemSelected() {
        mState = ActivityState.LIST_SEL_ONE;
        invalidateOptionsMenu();
    }

    @Override
    public void onSecondItemSelected() {
        mState = ActivityState.LIST_SEL_TWO;
        invalidateOptionsMenu();
    }

    @Override
    public void onNoItemSelected() {
        mState = ActivityState.LIST_NONE_SEL;
        invalidateOptionsMenu();
    }

    @Override
    public void onExecutionStart() {
        if (mState != ActivityState.EXECUTE) {
            mPrevState = mState;
        }
        mState = ActivityState.EXECUTE;
        fab.hide();
        invalidateOptionsMenu();
    }

    @Override
    public void onExecutionEnd() {
        mState = mPrevState;
        fab.show();
        supportInvalidateOptionsMenu();
        invalidateOptionsMenu();
    }

    private enum ActivityState {
        LIST_NONE_SEL, LIST_SEL_ONE, LIST_SEL_TWO, CREATE, EXECUTE;
    }
}
