package edu.wisc.cs.renderscriptanalyzer.Screens;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import edu.wisc.cs.renderscriptanalyzer.R;
import edu.wisc.cs.renderscriptanalyzer.Scripting.BenchmarkFile;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EArgs;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EAttributeSet;

/**
 * Created by Sam on 7/5/2016.
 * This dialog will represent information about an execution script, including parameter names, values,
 * and anything else relevant to that execution script.
 */
public class InfoDialog extends DialogFragment implements DialogInterface.OnShowListener {
    private View mRootView;
    private BenchmarkFile mFile;
    private LinearLayout mInfoLayout;
    private TextView mInfoTitle;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View view = LayoutInflater.from(this.getActivity()).inflate(R.layout.dialog_terminal, null);
        mRootView = view;
        builder.setTitle("Executing...").setNeutralButton("Dismiss", null).setCancelable(false).setView(view);

        // Create the AlertDialog object and return it
        final AlertDialog toReturn = builder.create();
        mInfoLayout = (LinearLayout) view.findViewById(R.id.layout_info);
        mInfoTitle = (TextView) view.findViewById(R.id.text_infoTitle);
        toReturn.setOnShowListener(this);
        return toReturn;
    }

    @Override
    public void onShow(DialogInterface dialog) {
    }

    /**
     * Sets the dialog to the specified file to display correct info
     * @param file The file for which to display the information
     */
    public void setExecutionFile(BenchmarkFile file){
        mFile = file;
        mInfoTitle.setText(file.getFilename());
        this.setList(file);
        this.invalidate();
    }

    private void setList(BenchmarkFile file){
        this.mInfoLayout.removeAllViews();
        List<EAttributeSet> attributeList = file.getAttributes();
        // Now we have all the attributes, start making views for each
        for (EAttributeSet attributeSet : attributeList){
            // Gather all the info for the attributes
            String benchmarkName = attributeSet.getBenchmarkName();
            int iterations = attributeSet.getIterations();
            EArgs benchmarkEArgs = attributeSet.getArgs();
            // TODO Actually create views and fill the dialog
            for (int i = 0; i < benchmarkEArgs.getArgCount(); i++){
                String argName = benchmarkEArgs.getArgNameAt(i);
                String argVal = benchmarkEArgs.getArgValueAt(i);
            }
        }
    }

    public void invalidate(){
        mRootView.invalidate();
    }
}
