package edu.wisc.cs.renderscriptanalyzer.Benchmarking;

import android.content.Context;
import android.renderscript.RenderScript;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import edu.wisc.cs.renderscriptanalyzer.Screens.BenchmarkActivityListener;
import edu.wisc.cs.renderscriptanalyzer.Scripting.BenchmarkFile;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EArgs;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EAttributeSet;

/**
 * Created by Sam on 3/10/2016.
 * This class represents a certain benchmark execution. This has been separated from the
 * actual view to allow for better scripting capabilities.
 */
public abstract class BenchmarkExecution {
    private BenchmarkActivityListener mListener;
    // A list of the last execution times
    private List<Long> mPreviousExecutionTimes;
    // The name of our benchmark
    protected String mBenchmarkName = "Unnamed Benchmark";
    // A description of our benchmark
    protected String mBenchmarkDescription = "No Description Available";
    // The output window to which to print the output
    private BenchmarkPrinter mPrinter;
    // This instance of EArgs represents legal arguments for our program. They should NOT
    // be used for executing benchmarks
    private EArgs mArgs;
    // Values that change during execution. DO NOT EDIT THESE YOURSELF
    private int mIterationsRemaining;
    private int mIterationNo;
    private ExecutionMode mMode;
    private EArgs mArgsInFlight;
    private EAttributeSet mAttributes;
    private boolean mRun = true;
    private String mMessageBuffer = "";
    // The results string
    private String mResultsString;
    // our renderscript context
    protected RenderScript mRenderScript;
    // The filepath for the serial executable
    protected String mFilepath;
    // Command line arguments
    protected String[] mArguments;
    // File that will be executed
    protected File mExeFile;
    // The process itself
    protected Process mProcess;

    // TODO allow this to execute with the view it represents

    public BenchmarkExecution() {
        mPreviousExecutionTimes = new ArrayList<Long>();
        mArgs = new EArgs();
        setupArgs();
    }

    public void setBenchmarkActivityListener(BenchmarkActivityListener l) {
        this.mListener = l;
    }

    public void setBenchmarkPrinter(BenchmarkPrinter p) {
        this.mPrinter = p;
    }

    /**
     * Prints a string followed by a newline character to the pre-assigned output
     *
     * @param toPrint The string to print, which will be followed by a newline
     */
    public void println(String toPrint) {
        mMessageBuffer = mMessageBuffer + toPrint + "\n";
    }

    /**
     * Prints a string to the pre-assigned output
     *
     * @param toPrint The string to print, which will NOT be followed by a newline
     */
    public void print(String toPrint) {
        mMessageBuffer = mMessageBuffer + toPrint;

    }

    public EArgs getArgs() {
        return mArgs;
    }

    /**
     * Gets a list of the previous execution times for the last execution. This list is NOT a copy
     * of the list used in the fragment, so make sure to copy it if you need to keep it!
     *
     * @return The pointer to the fragment's list of execution times
     */
    public List<Long> getPreviousExecutionTimes() {
        return mPreviousExecutionTimes;
    }

    /**
     * Gets the name of the current benchmark.
     *
     * @return The name of the benchmark.
     */
    public String getBenchmarkName() {
        return mBenchmarkName;
    }

    /**
     * Gets the description of the current benchmark.
     *
     * @return The description of the benchmark.
     */
    public String getBenchmarkDescription() {
        return mBenchmarkDescription;
    }

    /**
     * Called to determine if the supplied value is a valid argument name or not.
     * If the argument is not valid, an IllegalArgumentException is thrown
     *
     * @param argName The string of the argument name.
     * @param value   The argument value, cast as needed
     */
    public void validateArgument(String argName, Object value) throws IllegalArgumentException {
        if (this.isValidArgument(argName, value)) {
            return;
        }
        throw new IllegalArgumentException("Value for parameter " + argName + " is invalid!");
    }

    @Deprecated
    public void executeFileScript(BenchmarkFile script, Context c) {
        // open the results file here
        mResultsString = "";
        for (int i = 0; i < script.getNumAttributes(); i++) {
            this.executeTask(script.getAttribute(i), c);
        }

        // write the results here
    }

    /**
     * This method actually executes the task of the fragment, as well as collecting timing
     * statistics for it.
     *
     * @param attributes the attribute set to represent this execution
     */
    public void executeTask(final EAttributeSet attributes, Context c) {
        mRun = true;
        mIterationsRemaining = attributes.getIterations();
        mRenderScript = RenderScript.create(c);
        mIterationNo = 0;
        mMode = attributes.getExecutionMode();
        mArgsInFlight = attributes.getArgs();
        mAttributes = attributes;
        mPreviousExecutionTimes.clear();
        if (mPrinter != null) {
            mPrinter.println("");
            if (mMode == ExecutionMode.SERIAL) {
                mPrinter.println("Starting serial execution of " + mBenchmarkName + " benchmark.");
            } else {
                mPrinter.println("Starting renderscript execution of " + mBenchmarkName + " benchmark.");
            }

            mPrinter.println("");
        }

        //Log.e("Task Start", mArgsInFlight.toString());
        taskStart(mArgsInFlight, mIterationsRemaining);
        executeSingleIteration();

    }

    /**
     * Tells the execution to finish its current iteration and stop executing
     */
    public void interruptTask() {
        mRun = false;
    }

    private void executeSingleIteration() {
        mIterationsRemaining--;
        mIterationNo++;
        new Thread(new Runnable() {
            @Override
            public void run() {
                long beforeTime;
                long afterTime;
                long executionTimeNS;
                // Before we execute make sure we do the setup
                try {
                    doIterationSetup(mArgsInFlight);
                } catch (Exception e) {
                    println("Uncaught exception in serial setup!");
                    println(e.getMessage());
                }
                // Now that we've actually done the setup let's start the timer and task
                beforeTime = System.nanoTime();
                try {
                    doIterationWork(mArgsInFlight);
                } catch (Exception e) {
                    println("Uncaught exception in serial work!");
                    println(e.getMessage());
                }
                // Now stop the timer
                afterTime = System.nanoTime();
                // Finally cleanup
                try {
                    doIterationCleanup(mArgsInFlight);
                } catch (Exception e) {
                    println("Uncaught exception in serial cleanup!");
                    println(e.getMessage());
                }
                executionTimeNS = afterTime - beforeTime;
                mAttributes.addExecutionTime(executionTimeNS, mIterationNo - 1);
                mPreviousExecutionTimes.add(executionTimeNS);

                updateUI();
            }
        }).start();
    }

    private void updateUI() {
        BenchmarkManager.getInstance().currActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mListener != null) {
                    mListener.onTaskProgress(BenchmarkExecution.this, mIterationNo);
                }
                if (mPrinter != null) {
                    mPrinter.print(mMessageBuffer);
                    mMessageBuffer = "";
                }
                // once the UI is finished run another thread again
                if (mIterationsRemaining > 0 && mRun) {
                    executeSingleIteration();
                } else if (mIterationsRemaining > 0 && !mRun) {
                    if (mPrinter != null) {
                        mPrinter.println("");
                        mPrinter.println("Benchmark " + mBenchmarkName + " interrupted (" + mIterationNo + " iterations completed)");
                    }
                    taskInterrupted(mArgs);

                } else {
                    // once we're done call the listener
                    if (mPrinter != null) {
                        mPrinter.println("");
                        if (mMode == ExecutionMode.SERIAL) {
                            mPrinter.println("Serial benchmark " + mBenchmarkName + " finished!");
                        } else {
                            mPrinter.println("Renderscript benchmark " + mBenchmarkName + " finished!");
                        }

                    }
                    taskDone(mArgs);

                }
            }
        });
    }


    /**
     * This is an optional method to override. This method is for any setup that needs to be done
     * before executing the serial workload. For example, the ocean fragment might initialize
     * a grid here.
     */
    protected abstract void doIterationSetup(EArgs args);

    /**
     * Each fragment that extends this fragment will implement its serial method in this method.
     * For instance, the ocean fragment will implement serial ocean here.
     */
    protected abstract void doIterationWork(EArgs args);

    /**
     * This is an optional method to override. This method is for any cleanup that needs to be done
     * after executing the serial workload.
     */
    protected abstract void doIterationCleanup(EArgs args);

    /**
     * Use this method to specify the layout resource to inflate for this fragment.
     *
     * @return The resource you want to inflate.
     */
    protected abstract int getLayoutRes();

    /**
     * Called before the first execution begins.
     */
    protected abstract void doTotalSetup(EArgs args);


    /**
     * Called when the fragment is done with all iterations of its current execution.
     */
    protected abstract void doTotalCompletion(EArgs args);

    /**
     * Called when the execution is created. Allows the execution to add custom args.
     * When using the addArgument method, the argument's value will be the default value for
     * the argument.
     *
     * @param args
     */
    protected abstract void addProgramArguments(EArgs args);

    /**
     * Determines if a specified argument is valid or not (in ocean a valid argument for grid size
     * is a power of 2 plus 2)
     * Can be overridden by the child class if desired.
     *
     * @param argName Name of the argument as a string as it appears in EArgs
     * @param value   Value of the argument, cast as needed
     * @return True if the argument is valid, false otherwise
     */
    protected boolean isValidArgument(String argName, Object value) {
        return true;
    }

    private void taskStart(EArgs args, int iterations) {
        if (mListener != null) {
            mListener.onTaskBegin(this, iterations);
        }
        getProgramArgs(args);
        doTotalSetup(args);
    }

    /**
     * Called during setup to allow the execution class to parse its arguments
     *
     * @param args The arguments from which to parse
     */
    protected abstract void getProgramArgs(EArgs args);


    /**
     * Called when a task has completed. It's possible to override this, but you must make a
     * call to super first.
     */
    private void taskDone(EArgs args) {
        //Log.e("Task Done", mArgsInFlight.toString());
        doTotalCompletion(args);
        String data = mAttributes.toJSON().toString();
        //mResultsString = mResultsString + "\n" + data;
        //FileManager.getInstance().writeFile(this.getBenchmarkName(), data, false);
        if (mListener != null) {
            mListener.onTaskComplete(this, data);
        }
        //mRenderScript.destroy();
    }

    private void taskInterrupted(EArgs args) {
        if (mListener != null) {
            mListener.onTaskInterrupted(this, mIterationNo, mAttributes.toJSON().toString());
        }
        doTotalCompletion(args);
    }

    private void setupArgs() {
        addProgramArguments(mArgs);
    }
}
