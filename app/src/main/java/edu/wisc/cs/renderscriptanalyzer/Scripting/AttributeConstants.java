package edu.wisc.cs.renderscriptanalyzer.Scripting;

/**
 * Created by Sam on 3/10/2016.
 */
public class AttributeConstants {
    public static final String scriptAttr_benchmarkName = "BenchmarkName";
    public static final String scriptAttr_benchmarkArgs = "BenchmarkArgs";
    public static final String scriptAttr_numIterations = "NumIterations";
    public static final String scriptAttr_exeMode = "Execution Mode";
    public static final String scriptAttr_executionResultTimes = "ExecutionTimes";
    public static final String scriptAttr_argHeader = "Arguments";
    public static final String scriptAttr_argName = "ArgumentName";
    public static final String scriptAttr_argType = "ArgumentType";
    public static final String scriptAttr_argValue = "ArgumentValue";
}
