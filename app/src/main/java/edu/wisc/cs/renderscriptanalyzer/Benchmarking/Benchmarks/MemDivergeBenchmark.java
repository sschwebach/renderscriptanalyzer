package edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks;

import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.Script;
import android.renderscript.Type;

import java.util.Random;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkExecution;
import edu.wisc.cs.renderscriptanalyzer.ScriptC_divergence;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EArgs;

/**
 * Created by Sam on 5/8/2016.
 */
public class MemDivergeBenchmark extends BenchmarkExecution {
    private static String MEM_DIV_ITERATIONS = "Number of iterations";
    private static String MEM_DIV_ARRAYSIZE = "Array Size (Possible memory locations)";
    private static String MEM_DIV_DEBUG = "Debug Mode";
    private int mIterations;
    private int mArraySize;
    private boolean mDebug;
    private int[] mSourceArray;
    private int[] mDestArray;
    private Allocation mSourceAlloc;
    private Allocation mDestAlloc;
    private ScriptC_divergence mDiverge;

    @Override
    protected void doIterationSetup(EArgs args) {

    }

    @Override
    protected void doIterationWork(EArgs args) {
        // allocate the arrays
        mSourceArray = new int[mArraySize];
        mDestArray = new int[mIterations];
        // initialize the source array
        Random rng = new Random();
        for (int i = 0; i < mArraySize; i++){
            if (mDebug){
                mSourceArray[i] = i;
            }else{
                mSourceArray[i] = rng.nextInt();
            }
        }
        // allocate the allocations
        mSourceAlloc = Allocation.createTyped(mRenderScript, Type.createX(mRenderScript, Element.I32(mRenderScript), mArraySize));
        mDestAlloc = Allocation.createTyped(mRenderScript, Type.createX(mRenderScript, Element.I32(mRenderScript), mIterations));
        // copy the source array to its allocation
        mSourceAlloc.copy1DRangeFrom(0, mArraySize, mSourceArray);
        // create the renderscript
        mDiverge = new ScriptC_divergence(mRenderScript);
        // set globals
        mDiverge.set_arraySize(mArraySize);
        mDiverge.set_sourceArray(mSourceAlloc);
        // start renderscript
        Script.LaunchOptions lOptions = new Script.LaunchOptions();
        mDiverge.forEach_memDiverge(mDestAlloc, mDestAlloc, lOptions);
        // copy the data back
        mDestAlloc.copyTo(mDestArray);
    }

    @Override
    protected void doIterationCleanup(EArgs args) {
        // if in debug mode, print the results
        if (mDebug){
            println("Result grid");
            print("[");
            for (int i = 0; i < mIterations; i++){
                print(mDestArray[i] + ", ");
            }
            println("]");
        }
        // destroy our allocations
        mSourceAlloc.destroy();
        mDestAlloc.destroy();
    }

    @Override
    protected int getLayoutRes() {
        return 0;
    }

    @Override
    protected void doTotalSetup(EArgs args) {

    }

    @Override
    protected void doTotalCompletion(EArgs args) {

    }

    @Override
    protected void addProgramArguments(EArgs args) {
        mBenchmarkName = "Memory Divergence Benchmark";
        args.addArgument(MEM_DIV_ITERATIONS, 10);
        args.addArgument(MEM_DIV_ARRAYSIZE, 10);
        args.addArgument(MEM_DIV_DEBUG, false);
    }

    @Override
    protected void getProgramArgs(EArgs args) {
        mIterations = args.getInt(MEM_DIV_ITERATIONS, 10);
        mArraySize = args.getInt(MEM_DIV_ARRAYSIZE, 10);
        mDebug = args.getBoolean(MEM_DIV_DEBUG, false);
    }

}
