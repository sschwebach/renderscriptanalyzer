package edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks;

import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.Script;

import java.util.Random;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkExecution;
import edu.wisc.cs.renderscriptanalyzer.ScriptC_dft;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EArgs;

/**
 * Created by Sam on 4/27/2016.
 */
public class DFTBenchmark extends BenchmarkExecution {
    public static final String DFT_ARRAYSIZE = "Input Array Size";
    public static final String DFT_ITERATIONS = "Iterations";
    public static final String DFT_DEBUG = "Allow debug print statements";
    // array size
    private int mArraySize;
    // iterations
    private int mIterations;
    // Allow debugging?
    private boolean mDebug;
    // Array to hold our data
    private double[] mData;
    // Our DFT renderscript
    private ScriptC_dft mDFTScript;
    // Data allocation
    Allocation mInputAllocation;
    Allocation mOutputAllocation;

    @Override
    protected void doIterationSetup(EArgs args) {

    }

    void initData() {
        mData = new double[mArraySize * 2];
        Random rng = new Random();
        for (int i = 0; i < mArraySize * 2; i += 2) {
            if (mDebug) {
                mData[i] = i / 2 + 1;
                mData[i + 1] = i / 2 + 1;
            } else {
                mData[i] = rng.nextDouble();
                mData[i + 1] = rng.nextDouble();
            }
        }
        if (mDebug) {
            println("The initial values are:");
            print("[");
            for (int i = 0; i < mArraySize * 2; i += 2) {
                print("(" + mData[i] + ", " + mData[i + 1] + "), ");
            }
            println("]");
        }
    }

    @Override
    protected void doIterationWork(EArgs args) {
        // Allocate, initialize, and create our data allocation, as well as the script
        initData();
        mInputAllocation = Allocation.createSized(mRenderScript, Element.F64_2(mRenderScript), mArraySize);
        mOutputAllocation = Allocation.createSized(mRenderScript, Element.F64_2(mRenderScript), mArraySize);
        mInputAllocation.copyFromUnchecked(mData);
        mDFTScript = new ScriptC_dft(mRenderScript);
        mDFTScript.set_m(mArraySize);
        mDFTScript.set_data(mInputAllocation);
        Script.LaunchOptions lOptions = new Script.LaunchOptions();
        for (int i = 0; i < mIterations; i++) {
            mDFTScript.forEach_dft(mInputAllocation, mOutputAllocation, lOptions);
        }
    }

    @Override
    protected void doIterationCleanup(EArgs args) {
        mOutputAllocation.copyTo(mData);
        if (mDebug) {
            println("The final result is:");
            print("[");
            for (int i = 0; i < mArraySize * 2; i += 2) {
                print("(" + mData[i] + ", " + mData[i + 1] + "), ");
            }
            println("]");
        }
        mInputAllocation.destroy();
    }

    @Override
    protected int getLayoutRes() {
        return 0;
    }

    @Override
    protected void doTotalSetup(EArgs args) {

    }

    @Override
    protected void doTotalCompletion(EArgs args) {

    }

    @Override
    protected void addProgramArguments(EArgs args) {
        mBenchmarkName = "DFT Benchmark";
        args.addArgument(DFT_ARRAYSIZE, 512);
        args.addArgument(DFT_ITERATIONS, 1);
        args.addArgument(DFT_DEBUG, false);

    }

    @Override
    protected void getProgramArgs(EArgs args) {
        mArraySize = args.getInt(DFT_ARRAYSIZE, 512);
        mIterations = args.getInt(DFT_ITERATIONS, 1);
        mDebug = args.getBoolean(DFT_DEBUG, false);
    }

}
