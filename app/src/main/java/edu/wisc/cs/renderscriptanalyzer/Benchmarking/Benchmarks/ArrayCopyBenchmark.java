package edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks;

import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.Type;

import java.util.Random;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkExecution;
import edu.wisc.cs.renderscriptanalyzer.ScriptC_arrayCopy;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EArgs;

/**
 * Created by Sam on 5/7/2016.
 * This benchmark simply copies an array in memory. Unlike most of the other benchmarks, the serial
 * version is implemented in Java because we want a better idea of the memory overhead of transferring
 * data to the RenderScript runtime layer.
 */
public class ArrayCopyBenchmark extends BenchmarkExecution {
    private static final String COPY_GRIDSIZE = "Array Size";
    private static final String COPY_ITERATIONS = "Number of Iterations";
    private static final String COPY_ALLOCATE = "Test Allocation Instead of Copying";
    private static final String COPY_DEBUG = "Debug Mode";
    private int mArraySize;
    private int mIterations;
    private boolean mAllocationInsteadofCopy;
    private boolean mDebug;
    private int[] mOriginGrid;
    private int[] mDestinationGrid;
    private Allocation mOriginAlloc;
    private ScriptC_arrayCopy mArrayCopyScript;

    @Override
    protected void doIterationSetup(EArgs args) {
        println("Setting up renderscript array copy...");
        // Create the initial arrays
        mOriginGrid = new int[mArraySize];
        mDestinationGrid = new int[mArraySize];
        Random rng = new Random();
        for (int i = 0; i < mArraySize; i++) {
            if (mDebug) {
                mOriginGrid[i] = i;
            } else {
                mOriginGrid[i] = rng.nextInt();
            }
        }
        if (!mAllocationInsteadofCopy){
            mOriginAlloc = Allocation.createTyped(mRenderScript, Type.createX(mRenderScript, Element.I32(mRenderScript), mArraySize));
        }
    }

    @Override
    protected void doIterationWork(EArgs args) {
        if (mAllocationInsteadofCopy){
            for (int i = 0; i < mIterations; i++) {
                mOriginAlloc = Allocation.createTyped(mRenderScript, Type.createX(mRenderScript, Element.I32(mRenderScript), mArraySize));
            }
        } else {
            // now create the allocations we'll be passing to RenderScript
            for (int i = 0; i < mIterations; i++) {
                mOriginAlloc.copy1DRangeFrom(0, mArraySize, mOriginGrid);
                // at the end copy back the arrays
                mOriginAlloc.copyTo(mDestinationGrid);
            }
        }
    }

    @Override
    protected void doIterationCleanup(EArgs args) {
        if (mDebug) {
            print("[");
            for (int i = 0; i < mArraySize; i++) {
                print(mDestinationGrid[i] + ", ");
            }
            print("]");
        }
        mOriginAlloc.destroy();
    }

    @Override
    protected int getLayoutRes() {
        return 0;
    }

    @Override
    protected void doTotalSetup(EArgs args) {

    }

    @Override
    protected void doTotalCompletion(EArgs args) {

    }

    @Override
    protected void addProgramArguments(EArgs args) {
        mBenchmarkName = "Array Copy Benchmark";
        args.addArgument(COPY_GRIDSIZE, 4);
        args.addArgument(COPY_ITERATIONS, 1);
        args.addArgument(COPY_ALLOCATE, false);
        args.addArgument(COPY_DEBUG, false);
    }

    @Override
    protected void getProgramArgs(EArgs args) {
        mArraySize = args.getInt(COPY_GRIDSIZE, 4);
        mIterations = args.getInt(COPY_ITERATIONS, 1);
        mAllocationInsteadofCopy = args.getBoolean(COPY_ALLOCATE, false);
        mDebug = args.getBoolean(COPY_DEBUG, false);
    }

}
