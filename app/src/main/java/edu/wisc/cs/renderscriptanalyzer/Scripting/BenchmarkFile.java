package edu.wisc.cs.renderscriptanalyzer.Scripting;

import java.util.List;

/**
 * Created by samsc on 3/21/2016.
 * Represents a single benchmark file in our execution engine. Simply a wrapper for a few other
 * things, such as filename, and the script itself (EAttributeSet)
 */
public class BenchmarkFile {
    private List<EAttributeSet> mAttributes;
    private String mFilename;
    private boolean mSelected;

    /**
     * Constructor for the file.
     * @param attrs
     * @param filename
     */
    public BenchmarkFile(List<EAttributeSet> attrs, String filename){
        this.mAttributes = attrs;
        this.mFilename = filename;
        mSelected = false;
    }

    /**
     * Returns the name of the file without the file extension.
     * @return The filename of this file, without the extension.
     */
    public String getFilename(){
        return mFilename;
    }

    /**
     * Gets the attribute list of this particular file.
     * @return The list of all attribute sets for this file.
     */
    public List<EAttributeSet> getAttributes(){
        return mAttributes;
    }

    /**
     * Gets the number of attribute sets in this file (the number of executions)
     * @return The number of attribute sets, or size of the attribute list.
     */
    public int getNumAttributes(){
        return mAttributes.size();
    }

    /**
     * Gets the attribute set at the specified index of the list. Throws an exception if the index
     * is out of bounds.
     * @param index Index in the attribute list.
     * @return The attribute at the specified index.
     */
    public EAttributeSet getAttribute(int index){
        return mAttributes.get(index);
    }

    /**
     * Gets the current selction state of this file.
     * @return True if selected, false otherwise.
     */
    public boolean getSelected(){
        return mSelected;
    }

    /**
     * Sets the selection state of this file.
     * @param selected The new selection state for this file.
     */
    public void setSelected(boolean selected){
        this.mSelected = selected;
    }
}
