package edu.wisc.cs.renderscriptanalyzer.Benchmarking;

import android.app.ActionBar;
import android.database.DataSetObserver;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks.ArrayCopyBenchmark;
import edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks.BranchDivergeBenchmark;
import edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks.DFTBenchmark;
import edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks.ExecutableExecution;
import edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks.MatrixAddBenchmark;
import edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks.MatrixMultiplyBenchmark;
import edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks.MemDivergeBenchmark;
import edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks.OceanBenchmark;
import edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks.SerialOceanBenchmark;
import edu.wisc.cs.renderscriptanalyzer.Screens.BenchmarkActivity;
import edu.wisc.cs.renderscriptanalyzer.Widgets.ViewUtilities;

/**
 * Created by Sam on 3/14/2016.
 * This singleton class keeps a list of all valid benchmarks available to the application.
 */
public class BenchmarkManager implements SpinnerAdapter{
    // A list of available executions
    public List<BenchmarkExecution> executions;
    public BenchmarkActivity benchmarkActivity;
    public BenchmarkActivity currActivity;


    private static BenchmarkManager ourInstance = new BenchmarkManager();

    public static BenchmarkManager getInstance() {
        return ourInstance;
    }

    private BenchmarkManager() {
        executions = new ArrayList<BenchmarkExecution>();
        executions.add(new OceanBenchmark());
        executions.add(new ExecutableExecution());
        executions.add(new DFTBenchmark());
        executions.add(new MatrixAddBenchmark());
        executions.add(new MatrixMultiplyBenchmark());
        executions.add(new SerialOceanBenchmark());
        executions.add(new ArrayCopyBenchmark());
        executions.add(new BranchDivergeBenchmark());
        executions.add(new MemDivergeBenchmark());
        // TODO Add new executions here
    }

    public void init(BenchmarkActivity a){
        this.currActivity = a;
    }

    public static void setActivityHandle(BenchmarkActivity a) {
        ourInstance.benchmarkActivity = a;
    }

    /**
     * Finds and returns the first benchmark execution with the specified name, ignoring case.
     *
     * @param exeName The name of the desired execution. This is generally the execution's name.
     * @return The first execution object in the list whose name matches, ignoring case, or null if no execution is found
     */
    public BenchmarkExecution find(String exeName) {
        for (BenchmarkExecution exe : ourInstance.executions){
            if (exe.getBenchmarkName().equalsIgnoreCase(exeName)){
                return exe;
            }
        }
        return null;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        // TODO
        TextView returnView = (TextView) convertView;
        if (returnView == null){
            returnView = new TextView(parent.getContext());
        }
        returnView.setMinHeight((int) ViewUtilities.convertDpToPixel(60, parent.getContext()));
        returnView.setLayoutParams(new Spinner.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT));
        returnView.setText(ourInstance.executions.get(position).getBenchmarkName());
        returnView.setGravity(Gravity.CENTER_VERTICAL);
        returnView.setTextSize(18);
        returnView.setPadding((int) ViewUtilities.convertDpToPixel(8, parent.getContext()), 0, 0, 0);
        //returnView.setTextSize(R.dimen.text_size_standard);
        return returnView;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return ourInstance.executions.size();
    }

    @Override
    public Object getItem(int position) {
        return ourInstance.executions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO
        TextView returnView = (TextView) convertView;
        if (returnView == null){
            returnView = new TextView(parent.getContext());
        }
        returnView.setMinHeight((int) ViewUtilities.convertDpToPixel(60, parent.getContext()));
        returnView.setLayoutParams(new Spinner.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT));
        returnView.setText(ourInstance.executions.get(position).getBenchmarkName());
        returnView.setGravity(Gravity.CENTER_VERTICAL);
        returnView.setTextSize(18);
        return returnView;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
