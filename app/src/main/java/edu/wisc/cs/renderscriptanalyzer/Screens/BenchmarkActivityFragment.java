package edu.wisc.cs.renderscriptanalyzer.Screens;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkExecution;

/**
 * Created by Sam on 3/10/2016.
 * A fragment template that all other fragments within the activity must use.
 */
public abstract class BenchmarkActivityFragment extends Fragment {
    private BenchmarkActivityListener mListener;
    // The name of our benchmark
    protected String mBenchmarkName = "Unnamed Benchmark";
    // A description of our benchmark
    protected String mBenchmarkDescription = "No Description Available";
    // The current execution being performed
    private BenchmarkExecution mCurrExecution;
    protected BenchmarkActivity mActivity;

    public void attach(Activity activity){
        this.mActivity = (BenchmarkActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View toReturn = inflater.inflate(this.getLayoutRes(), container, false);
        setupView(toReturn);
        return toReturn;
    }

    /**
     * Called when the action button for the overall activity is pressed.
     */
    public abstract void onActionButtonPressed();

    /**
     * Called when the fragment gains focus in our activity
     */
    public abstract void onFragmentFocused();

    public abstract int getLayoutRes();

    public abstract void setupView(View v);

}
