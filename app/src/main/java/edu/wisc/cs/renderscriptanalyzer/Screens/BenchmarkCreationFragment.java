package edu.wisc.cs.renderscriptanalyzer.Screens;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkExecution;
import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkManager;
import edu.wisc.cs.renderscriptanalyzer.Benchmarking.ExecutionMode;
import edu.wisc.cs.renderscriptanalyzer.Benchmarking.FileManager;
import edu.wisc.cs.renderscriptanalyzer.R;
import edu.wisc.cs.renderscriptanalyzer.Scripting.AttributeConstants;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EArgs;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EAttributeSet;
import edu.wisc.cs.renderscriptanalyzer.Widgets.ArgumentView;

/**
 * Created by Sam on 3/21/2016.
 */
public class BenchmarkCreationFragment extends BenchmarkActivityFragment {
    private EditText mNameText;
    private EditText mIterationText;
    private LinearLayout mParamLayout;
    private BenchmarkExecution mExecution;
    private Context mContext;
    private Spinner mBenchmarkSpinner;
    private Button mSaveButton;

    @Override
    public void onActionButtonPressed() {

    }

    @Override
    public void onFragmentFocused() {
        ((Toolbar)mActivity.findViewById(R.id.toolbar)).setTitle("Create a New Script");
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_scriptcreate;
    }

    @Override
    public void setupView(View v) {
        mNameText = (EditText) v.findViewById(R.id.edittext_filename);
        mIterationText = (EditText) v.findViewById(R.id.edittext_iterations);
        mParamLayout = (LinearLayout) v.findViewById(R.id.layout_paramContainer);
        // set it to the first execution for the time being
        mExecution = BenchmarkManager.getInstance().executions.get(0);
        mContext = v.getContext();
        mSaveButton = (Button) v.findViewById(R.id.button_scriptCreate);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveScript();
            }
        });
        mBenchmarkSpinner = (Spinner) v.findViewById(R.id.spinner_benchmarkChoose);
        mBenchmarkSpinner.setAdapter(BenchmarkManager.getInstance());
        mBenchmarkSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mExecution = (BenchmarkExecution) mBenchmarkSpinner.getAdapter().getItem(position);
                setupParamViews(mExecution);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // TODO make our views here

        // Once we've setup our initial execution, set it up
        setupParamViews(mExecution);
    }

    private void setupParamViews(BenchmarkExecution exe){
        mParamLayout.removeAllViews();
        for (int i = 0; i < exe.getArgs().getArgCount(); i++){
            try {
                JSONObject currObject = (JSONObject) exe.getArgs().getJSONObject().get(i);
                ArgumentView newArgView = new ArgumentView(mContext, mExecution.getBenchmarkName(), currObject.getString(AttributeConstants.scriptAttr_argName), (EArgs.ParamType) currObject.get(AttributeConstants.scriptAttr_argType), currObject.get(AttributeConstants.scriptAttr_argValue));
                mParamLayout.addView(newArgView);
            } catch (JSONException e){

            }
        }
    }

    private void saveScript(){
        // TODO make our script
        try{
            String benchmarkName = mExecution.getBenchmarkName();
            String scriptName = getScriptName();
            int iterations = getIterations();
            ExecutionMode exeMode = ExecutionMode.SERIAL;
            EArgs newArguments = new EArgs();
            // get all the mArguments from the argument views
            for (int i = 0; i < mParamLayout.getChildCount(); i++){
                ArgumentView argument = (ArgumentView) mParamLayout.getChildAt(i);
                argument.addArgument(newArguments);
            }
            EAttributeSet newAttributes = new EAttributeSet(newArguments, benchmarkName, iterations, exeMode);
            // save the file
            FileManager.getInstance().writeFile(scriptName, newAttributes.toJSON().toString(), true);
            // Once we've succeeded, make a toast message and leave the script creator
            Toast.makeText(this.mContext, "Script " + scriptName + " saved", Toast.LENGTH_SHORT).show();
            this.getActivity().onBackPressed();
        } catch (IllegalArgumentException e){
            // Make a dialog telling the user which argument is illegal
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(e.getMessage())
                    .setNeutralButton("Dismiss", null)
                    .show();
        }
    }

    private int getIterations() throws IllegalArgumentException {
        int iterations;
        try {
            iterations = Integer.parseInt(mIterationText.getText().toString());
        } catch (NumberFormatException e){
            throw new IllegalArgumentException("Number of iterations is invalid!");
        }
        if (iterations < 1){
            throw new IllegalArgumentException("Number of iterations must be greater than zero!");
        }
        return iterations;
    }

    private String getScriptName() throws IllegalArgumentException{
        String name = mNameText.getText().toString();
        if (name.equals("")){
            throw new IllegalArgumentException("You must specify a script file name!");
        }
        return name;
    }
}
