package edu.wisc.cs.renderscriptanalyzer.Benchmarking;

/**
 * This exception is thrown when an execution is cancelled. We'll see if we actually use this
 * or not...
 * Created by Sam on 3/14/2016.
 */
public class BenchmarkInterruptException extends Exception {

}
