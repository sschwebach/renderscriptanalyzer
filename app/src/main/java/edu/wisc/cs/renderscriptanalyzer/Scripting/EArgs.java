package edu.wisc.cs.renderscriptanalyzer.Scripting;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sam on 3/10/2016.
 * Represents the mArguments to a program of a single execution that are unique to that program.
 * An example for the ocean program would be the grid size.
 * The mArguments are represented as a JSONArray, where each argument has a name and value.
 */
public class EArgs {
    private JSONArray mArgs;

    public EArgs() {
        this.mArgs = new JSONArray();
    }

    public EArgs(JSONArray args) {
        this.mArgs = args;
    }

    /**
     * Gets the number of special parameters for this benchmark, or zero if there are none
     *
     * @return
     */
    public int getArgCount() {
        if (mArgs != null) {
            return mArgs.length();
        }
        return 0;
    }

    /**
     * Gets the name of the argument at index index
     * @param index Index of the eargs name
     * @return The name of the argument, as a string
     */
    public String getArgNameAt(int index) {
        try {
            return mArgs.getJSONObject(index).getString(AttributeConstants.scriptAttr_argName);
        } catch (JSONException e) {
            return "";
        }
    }

    /**
     * Gets the value of the argument at index index
     * @param index Index of the eargs value
     * @return The value of the argument, as a string
     */
    public String getArgValueAt(int index) {
        try {
            return mArgs.getJSONObject(index).getString(AttributeConstants.scriptAttr_argValue);
        } catch (JSONException e) {
            return "";
        }
    }

    public int getInt(String key, int defaultVal) {
        int toReturn = defaultVal;
        try {
            JSONObject argument = find(key);
            if (argument != null)
                toReturn = argument.getInt(AttributeConstants.scriptAttr_argValue);
        } catch (JSONException e) {
            // we couldn't find the specified key
        }
        return toReturn;
    }

    public String getString(String key, String defaultVal) {
        String toReturn = defaultVal;
        try {
            JSONObject argument = find(key);
            if (argument != null)
                toReturn = argument.getString(AttributeConstants.scriptAttr_argValue);
        } catch (JSONException e) {
            // we couldn't find the specified key
        }
        return toReturn;
    }

    public boolean getBoolean(String key, boolean defaultVal) {
        boolean toReturn = defaultVal;
        try {
            JSONObject argument = find(key);
            if (argument != null)
                toReturn = argument.getBoolean(AttributeConstants.scriptAttr_argValue);
        } catch (JSONException e) {
            // we couldn't find the specified key
        }
        return toReturn;
    }

    public double getDouble(String key, double defaultVal) {
        double toReturn = defaultVal;
        try {
            JSONObject argument = find(key);
            if (argument != null)
                toReturn = argument.getDouble(AttributeConstants.scriptAttr_argValue);
        } catch (JSONException e) {
            // we couldn't find the specified key
        }
        return toReturn;
    }

    public long getLong(String key, long defaultVal) {
        long toReturn = defaultVal;
        try {
            JSONObject argument = find(key);
            if (argument != null)
                toReturn = argument.getLong(AttributeConstants.scriptAttr_argValue);
        } catch (JSONException e) {
            // we couldn't find the specified key
        }
        return toReturn;
    }

    public void addArgument(String key, int value) {
        try {
            JSONObject newArg = new JSONObject();
            newArg.put(AttributeConstants.scriptAttr_argName, key);
            newArg.put(AttributeConstants.scriptAttr_argType, ParamType.INT);
            newArg.put(AttributeConstants.scriptAttr_argValue, value);
            mArgs.put(newArg);
        } catch (JSONException e) {
            // TODO do anything?
        }
    }

    public void addArgument(String key, String value) {
        try {
            JSONObject newArg = new JSONObject();
            newArg.put(AttributeConstants.scriptAttr_argName, key);
            newArg.put(AttributeConstants.scriptAttr_argType, ParamType.STRING);
            newArg.put(AttributeConstants.scriptAttr_argValue, value);
            mArgs.put(newArg);
        } catch (JSONException e) {
            // TODO do anything?
        }
    }

    public void addArgument(String key, boolean value) {
        try {
            JSONObject newArg = new JSONObject();
            newArg.put(AttributeConstants.scriptAttr_argName, key);
            newArg.put(AttributeConstants.scriptAttr_argType, ParamType.BOOLEAN);
            newArg.put(AttributeConstants.scriptAttr_argValue, value);
            mArgs.put(newArg);
        } catch (JSONException e) {
            // TODO do anything?
        }
    }

    public void addArgument(String key, double value) {
        try {
            JSONObject newArg = new JSONObject();
            newArg.put(AttributeConstants.scriptAttr_argName, key);
            newArg.put(AttributeConstants.scriptAttr_argType, ParamType.DOUBLE);
            newArg.put(AttributeConstants.scriptAttr_argValue, value);
            mArgs.put(newArg);
        } catch (JSONException e) {
            // TODO do anything?
        }
    }

    public void addArgument(String key, long value) {
        try {
            JSONObject newArg = new JSONObject();
            newArg.put(AttributeConstants.scriptAttr_argName, key);
            newArg.put(AttributeConstants.scriptAttr_argType, ParamType.LONG);
            newArg.put(AttributeConstants.scriptAttr_argValue, value);
            mArgs.put(newArg);
        } catch (JSONException e) {
            // TODO do anything?
        }
    }

    private JSONObject find(String key) {
        try {
            for (int i = 0; i < mArgs.length(); i++) {
                JSONObject candidate = mArgs.getJSONObject(i);
                String candKey = candidate.getString(AttributeConstants.scriptAttr_argName);
                if (candKey.equalsIgnoreCase(key)) {
                    return candidate;
                }
            }
        } catch (JSONException e) {
            // TODO do anything?
        }
        return null;
    }

    public enum ParamType {
        INT, STRING, BOOLEAN, DOUBLE, LONG;
    }

    public JSONArray getJSONObject() {
        return mArgs;
    }
}
