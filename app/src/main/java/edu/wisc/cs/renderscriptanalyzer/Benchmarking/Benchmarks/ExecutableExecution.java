package edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkExecution;
import edu.wisc.cs.renderscriptanalyzer.Benchmarking.FileManager;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EArgs;

/**
 * Created by samsc on 4/18/2016.
 * This class allows the user to run a generic executable located on the device's filesystem.
 * The user simply needs to specify the filepath plus any additional command line mArguments they wish.
 * It is possible to extend this class, but the base class will only perform meaningful work
 * when done in serial mode.
 */
public class ExecutableExecution extends BenchmarkExecution {
    // TODO this class is now a giant TODO
    private static final String EXE_PATH = "Executable Path";
    private static final String EXE_ARGS = "Additional Command Line Arguments";
    private static final String EXE_DEFUALT_PATH = "Add /data/local/tmp/ to specified path";
    // Command line mArguments for the executable
    protected String mCLArgs;
    // If true, add the default storage path (documents folder) to the executable path
    protected boolean mUseDefaultPath;

    @Override
    protected void doIterationSetup(EArgs args) {
        String[] argArray = setSerialCLArgs();
        mArguments = new String[argArray.length + 1];
        mArguments[0] = mExeFile.getAbsolutePath();
        // add all our mArguments into the array
        System.arraycopy(argArray, 0, mArguments, 1, argArray.length);
    }

    @Override
    protected void doIterationWork(EArgs args) {
        // execute the serial process
        try {

            //Process p = Runtime.getRuntime().exec("ls /data/local/tmp");
            mProcess = Runtime.getRuntime().exec(mArguments);
            mProcess.waitFor();

        } catch (IOException e) {
            println("Error! Couldn't execute " + mExeFile.getPath());
            println(e.getLocalizedMessage().toString());
        } catch (InterruptedException e) {
            println("Error! Got an interrupt exception???");
            println(e.getLocalizedMessage().toString());
        }
    }

    @Override
    protected void doIterationCleanup(EArgs args) {
        String line;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(mProcess.getInputStream()));

            StringBuilder log = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                println(line);
            }
        } catch (Exception e) {
            println("Error getting executable's output:");
            println(e.getLocalizedMessage());
        }

    }

    @Override
    protected int getLayoutRes() {
        return 0;
    }

    @Override
    protected void doTotalSetup(EArgs args) {
        // Before we begin take the executable from the tmp directory and copy it into the
        // application's directory where it can actually be executed
        moveFile();
    }


    @Override
    protected void doTotalCompletion(EArgs args) {

    }

    @Override
    protected void addProgramArguments(EArgs args) {
        mBenchmarkName = "Serial Execution Benchmark";
        args.addArgument(EXE_PATH, "Specify path here");
        args.addArgument(EXE_ARGS, "");
        args.addArgument(EXE_DEFUALT_PATH, false);
    }

    @Override
    public boolean isValidArgument(String argName, Object value) {
        if (argName.equalsIgnoreCase(EXE_PATH)) {
            // TODO find how to get a valid path
            return true;
        }
        // We have no way to know if the command line mArguments are correct
        return true;
    }

    @Override
    protected void getProgramArgs(EArgs args) {
        mFilepath = args.getString(EXE_PATH, "");
        mCLArgs = args.getString(EXE_ARGS, "");
        mUseDefaultPath = args.getBoolean(EXE_DEFUALT_PATH, false);
    }

    protected String[] setSerialCLArgs() {
        String[] args = mCLArgs.split(" ");
        return args;
    }

    protected String getSerialExecutablePath() {
        if (mUseDefaultPath) {
            return "/data/local/tmp/" + mFilepath;
        }
        return mFilepath;
    }

    /**
     * Moves the filepath to an executable location
     */
    protected void moveFile() {
        mFilepath = getSerialExecutablePath();
        mExeFile = new File(mFilepath);
        String fileName = mExeFile.getName();
        // copy the file into a path so that it can be executed after performing chmod on it
        try {
            Runtime.getRuntime().exec("chmod 777 " + mExeFile.getAbsolutePath());
            String copyString = "cp " + mExeFile.getAbsolutePath() + " " + FileManager.getInstance().getExecutableFilepath() + "/" + fileName;
            Runtime.getRuntime().exec(copyString);
            mExeFile = new File(FileManager.getInstance().getExecutableFilepath() + "/" + fileName);
            String modString = "chmod 777 " + mExeFile.getAbsolutePath();
            Runtime.getRuntime().exec(modString);
            Log.e("File", "File moving success!");
        } catch (IOException e) {
            println(e.getLocalizedMessage());
        }
    }

}
