package edu.wisc.cs.renderscriptanalyzer.Benchmarking;

/**
 * Created by Sam on 3/14/2016.
 */
public interface BenchmarkPrinter {
    public void println(String output);

    public void print(String output);
}
