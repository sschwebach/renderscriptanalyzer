package edu.wisc.cs.renderscriptanalyzer.Screens;

import android.app.Activity;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import edu.wisc.cs.renderscriptanalyzer.R;
import edu.wisc.cs.renderscriptanalyzer.Scripting.BenchmarkFile;

/**
 * Created by samsc on 3/21/2016.
 */
public class BenchmarkListAdapter extends BaseAdapter {
    private List<BenchmarkFile> mFiles;
    private Context mContext;

    public BenchmarkListAdapter(Context c, List<BenchmarkFile> files){
        this.mFiles = files;
        this.mContext = c;
    }

    @Override
    public int getCount() {
        return mFiles.size();
    }

    @Override
    public Object getItem(int position) {
        return mFiles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View toReturn = convertView;
        if (toReturn == null){
            // TODO inflate a view to return instead
            toReturn = View.inflate(mContext, R.layout.listview_benchmark, null);
        }
        // now make all our stuff
        final CheckBox selected = (CheckBox) toReturn.findViewById(R.id.check_selected);
        TextView title = (TextView) toReturn.findViewById(R.id.text_fileName);
        ImageView info = (ImageView) toReturn.findViewById(R.id.image_info);
        selected.setChecked(mFiles.get(position).getSelected());
        selected.setClickable(false);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO create a new interface to alert the list fragment that we pressed the
                // info button on a certain item
            }
        });
        title.setText(mFiles.get(position).getFilename().substring(0, mFiles.get(position).getFilename().length() - 4));
        // TODO set the onclick listener
        info.setOnClickListener(null);
        toReturn.setClickable(false);
        return toReturn;
    }

    public void onItemClicked(int position, View v){
        mFiles.get(position).setSelected(!mFiles.get(position).getSelected());
        v.invalidate();
    }
}
