package edu.wisc.cs.renderscriptanalyzer.Screens;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import edu.wisc.cs.renderscriptanalyzer.R;
import edu.wisc.cs.renderscriptanalyzer.Widgets.TerminalView;

/**
 * This dialog is displayed while the user is running a benchmark. It displays the output of
 * a benchmark in a terminal-like window in case debugging is needed.
 * Created by samsc on 3/28/2016.
 */
public class ExecutionDialog extends DialogFragment implements DialogInterface.OnShowListener {
    private CheckBox mCloseOnDoneCheck;
    private TextView mCloseOnDoneText;
    private TerminalView mOutputView;
    private ProgressBar mProgress;
    private ExecutionDialogListener mDialogListener;
    public boolean done = false;
    private View mRootView;

    public interface ExecutionDialogListener {
        public void onDialogShow();
        public void onDialogCancel();
        public void onDialogClose();
    }

    public void setExecutionDialogListener(ExecutionDialogListener l) {
        this.mDialogListener = l;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View view = LayoutInflater.from(this.getActivity()).inflate(R.layout.dialog_terminal, null);
        mRootView = view;
        builder.setTitle("Executing...").setNeutralButton("Cancel", null).setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return keyCode == KeyEvent.KEYCODE_BACK;
            }
        }).setCancelable(false).setView(view);

        // Create the AlertDialog object and return it
        final AlertDialog toReturn = builder.create();
        mCloseOnDoneCheck = (CheckBox) view.findViewById(R.id.check_closeOnFinish);
        // Make the text affect the checkbox as well
        mCloseOnDoneText = (TextView) view.findViewById(R.id.text_closeOnFinish);
        mCloseOnDoneText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCloseOnDoneCheck.setChecked(!mCloseOnDoneCheck.isChecked());
            }
        });
        mOutputView = (TerminalView) view.findViewById(R.id.terminal_outputView);
        mProgress = (ProgressBar) view.findViewById(R.id.progress_executionProgress);
        toReturn.setOnShowListener(this);
        return toReturn;
    }

    @Override
    public void onShow(DialogInterface dialog) {
        done = false;
        ((AlertDialog) this.getDialog()).getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(Color.BLACK);
        if (ExecutionDialog.this.mDialogListener != null) {
            ExecutionDialog.this.mDialogListener.onDialogShow();
        }

        Button action = ((AlertDialog) this.getDialog()).getButton(AlertDialog.BUTTON_NEUTRAL);
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ExecutionDialog.this.mDialogListener != null) {
                    if (done) {
                        ExecutionDialog.this.mDialogListener.onDialogClose();
                        ExecutionDialog.this.dismiss();
                    } else {
                        // Tell the task to cancel
                        ExecutionDialog.this.mDialogListener.onDialogCancel();
                    }
                }
            }
        });
    }

    public void invalidate(){
        mRootView.invalidate();
    }

    /**
     * Call this method to indicate to our dialog that the execution is done.
     */
    public void onExecutionDone() {
        done = true;
        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // If the user wants to close on finish, do so here
                if (mCloseOnDoneCheck.isChecked()) {
                    if (mDialogListener != null) {
                        mDialogListener.onDialogClose();
                    }
                    ExecutionDialog.this.dismiss();
                } else {
                    mCloseOnDoneCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                if (mDialogListener != null) {
                                    mDialogListener.onDialogClose();
                                }
                                ExecutionDialog.this.dismiss();
                            }
                        }
                    });
                    // TODO make a button visible that allows the user to dismiss the dialog manually
                    ((AlertDialog) ExecutionDialog.this.getDialog()).getButton(AlertDialog.BUTTON_NEUTRAL).setEnabled(true);
                    ((AlertDialog) ExecutionDialog.this.getDialog()).getButton(AlertDialog.BUTTON_NEUTRAL).setText("Close");
                }
            }
        });

    }

    /**
     * Prints a string to the terminal view, followed by a newline.
     *
     * @param line The line to print.
     */
    public void println(String line) {
        this.mOutputView.println(line);
    }

    /**
     * Prints a string to a terminal view, but no newline afterwards.
     *
     * @param text The string to print.
     */
    public void print(String text) {
        this.mOutputView.print(text);
    }

    /**
     * Clears the terminal printout.
     */
    public void clear() {
        this.mOutputView.clearText();
    }

    /**
     * Sets the maximum progress value for our progress bar. This will usually be the number
     * of iterations for the current benchmark.
     *
     * @param maxProgress
     */
    public void setProgressMax(int maxProgress) {
        mProgress.setMax(maxProgress);
        mProgress.setProgress(0);
    }

    /**
     * Updates the progress of our progress bar.
     *
     * @param value The value for the progress bar. Should be between 0 and the max value (will probably represent the number of iterations completed)
     */
    public void setProgress(int value) {
        mProgress.setProgress(value);
    }

}
