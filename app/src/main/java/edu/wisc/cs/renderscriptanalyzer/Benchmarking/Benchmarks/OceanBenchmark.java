package edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks;

import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.Script;
import android.renderscript.Type;
import android.util.Log;

import java.util.Random;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkExecution;
import edu.wisc.cs.renderscriptanalyzer.ScriptC_ocean;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EArgs;

/**
 * Created by Sam on 4/4/2016.
 */
public class OceanBenchmark extends BenchmarkExecution {
    private static final String OCEAN_X = "X Dimension (power of 2 plus 2)";
    private static final String OCEAN_Y = "Y Dimension (power of 2 plus 2)";
    private static final String OCEAN_TIMESTEPS = "Number of Timesteps (nonzero)";
    private static final String OCEAN_RAND = "Use random initialization";
    private int mXDim;
    private int mYDim;
    private int mTimesteps;
    private int mRand;
    private int[] grid1;
    private int[] grid2;
    ScriptC_ocean ocean;
    Allocation grid1Alloc;
    Allocation grid2Alloc;

    @Override
    protected void doIterationSetup(EArgs args) {
        // TODO allocate our ocean grid, get the number of timesteps and dimensions
        println("Setting up renderscript ocean...");

    }

    @Override
    protected void doIterationWork(EArgs args) {
        ocean = new ScriptC_ocean(mRenderScript);
        // TODO call the RS ocean function
        initGrids();
        grid1Alloc = Allocation.createTyped(mRenderScript, Type.createX(mRenderScript, Element.I32(mRenderScript), mXDim * mYDim));
        grid2Alloc = Allocation.createTyped(mRenderScript, Type.createX(mRenderScript, Element.I32(mRenderScript), mXDim * mYDim));
        grid1Alloc.copy1DRangeFrom(0, mXDim * mYDim, grid1);
        grid2Alloc.copy1DRangeFrom(0, mXDim * mYDim, grid2);
        ocean.set_xDim(mXDim);
        ocean.set_yDim(mYDim);
        ocean.set_grid1(grid1Alloc);
        ocean.set_grid2(grid1Alloc);
        Script.LaunchOptions lOptions = new Script.LaunchOptions();
        boolean odd = true;

        for (int i = 0; i < mTimesteps; i++) {
            if (odd) {
                ocean.set_odd(1);
                ocean.forEach_ocean(grid1Alloc, grid2Alloc, lOptions);
            } else {
                ocean.set_odd(0);
                ocean.forEach_ocean(grid2Alloc, grid1Alloc, lOptions);
            }
        }
        // copy the data back and read our outputs
        grid1Alloc.copyTo(grid1);
        grid2Alloc.copyTo(grid2);

    }

    @Override
    protected void doIterationCleanup(EArgs args) {

        // debug printing
        if (mRand <= 0) {
            if (mTimesteps % 2 == 0) {
                printGrid(grid1, mXDim, mYDim);
            } else {
                printGrid(grid2, mXDim, mYDim);
            }
        }
        // finally delete the allocations
        grid1Alloc.destroy();
        grid2Alloc.destroy();
    }

    @Override
    protected int getLayoutRes() {
        // turns out we never use this...
        return 0;
    }

    @Override
    protected void doTotalSetup(EArgs args) {

        // TODO get the correct filepath


        // TODO if there is anything we need to do a single time before executing all iterations, do
        // it here. It is NOT for steps that need to be done in a single iterration

    }

    @Override
    protected void doTotalCompletion(EArgs args) {
        // TODO final cleanup
        // meh
    }

    @Override
    protected void addProgramArguments(EArgs args) {
        mBenchmarkName = "Ocean Benchmark";
        // TODO add program mArguments such as grid size, number of iterations, etc
        args.addArgument(OCEAN_X, 4);
        args.addArgument(OCEAN_Y, 4);
        args.addArgument(OCEAN_TIMESTEPS, 2);
        args.addArgument(OCEAN_RAND, false);

    }

    @Override
    public boolean isValidArgument(String argName, Object value) {
        switch (argName) {
            case OCEAN_TIMESTEPS:
                return (int) value > 0;
            case OCEAN_Y:
            case OCEAN_X:
                return isPowerOfTwoPlusTwo((int) value);

        }
        return true;
    }

    @Override
    protected void getProgramArgs(EArgs args) {
        mXDim = args.getInt(OCEAN_X, 4);
        mYDim = args.getInt(OCEAN_Y, 4);
        mTimesteps = args.getInt(OCEAN_TIMESTEPS, 1);
        mRand = args.getBoolean(OCEAN_RAND, false) ? 1 : 0;
    }

    private boolean isPowerOfTwoPlusTwo(int value) {
        return ((value - 2) & (value - 3)) == 0;
    }

    /**
     * Initialize our grids depending on if we're doing random grid allocation or not
     */
    private void initGrids() {
        grid1 = new int[mXDim * mYDim];
        grid2 = new int[mXDim * mYDim];
        Random gridRand = new Random();
        for (int i = 0; i < mXDim; i++) {
            for (int j = 0; j < mYDim; j++) {
                if (mRand > 0) {
                    grid1[i * mXDim + j] = gridRand.nextInt();
                    grid2[i * mXDim + j] = grid1[i * mXDim + j];
                } else {
                    if (i == 0 || j == 0 || i == mXDim - 1 || j == mYDim - 1) {
                        grid1[i * mXDim + j] = 1000;
                        grid2[i * mXDim + j] = 1000;
                    } else {
                        grid1[i * mXDim + j] = 500;
                        grid2[i * mXDim + j] = 500;
                    }
                }
            }
        }
    }

    private void printGrid(int[] grid, int xDim, int yDim) {
        for (int i = 0; i < xDim; i++) {
            for (int j = 0; j < yDim; j++) {
                print(grid[i * xDim + j] + ", ");
            }
            if (i < xDim) {
                println("");
            }
        }
        println("");
    }

    /**
     * Flattens a 2D array into a 1D array that can be passed into renderscript
     * @param in The 2D int array to be flattened
     * @return A new 1D array representing the flattened 2D array
     */
    static private int[] flattenArray(int[][] in) {
        int[] toReturn = new int[in.length * in[0].length];
        for (int i = 0; i < in.length; i++) {
            for (int j = 0; j < in[0].length; j++) {
                toReturn[i * in.length + j] = in[i][j];
            }
        }
        return toReturn;
    }

    /**
     * Unflattens a 1D array into a 2D array
     * @param src
     * @param dest
     */
    static private void unFlattenArray(int[] src, int[][] dest){
        // make sure the sizes are the same
        if (src.length != dest.length * dest[0].length){
            Log.e("UnFlatten", "Source array of size " + src.length + " not equal to destination array of size " + dest.length * dest[0].length);
            return;
        }
        for (int i = 0; i < src.length; i++){
            dest[i / dest.length][i % dest.length] = src[i];
        }
    }

}
