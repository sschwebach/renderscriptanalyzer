package edu.wisc.cs.renderscriptanalyzer.Screens;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;

import java.util.Iterator;
import java.util.List;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkExecution;
import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkManager;
import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkPrinter;
import edu.wisc.cs.renderscriptanalyzer.Benchmarking.FileManager;
import edu.wisc.cs.renderscriptanalyzer.R;
import edu.wisc.cs.renderscriptanalyzer.Scripting.BenchmarkFile;

/**
 * Created by Sam on 3/21/2016.
 * Represents the fragment from which we will execute our benchmarks. Displays a list of all
 * our fragments and allows the user to execute them.
 */
public class BenchmarkListFragment extends BenchmarkActivityFragment implements BenchmarkPrinter, BenchmarkActivityListener, ExecutionDialog.ExecutionDialogListener {
    private List<BenchmarkFile> mBenchmarks;
    private ListView mListView;
    private ExecutionDialog mDialog;
    private InfoDialog mInfoDialog;
    private int mNumSelected;
    private int mTaskInFile;
    private String mData = "";
    private BenchmarkFile mCurrScript;
    private BenchmarkListListener mListListener;
    private SwipeRefreshLayout mSwipeLayout;
    private BenchmarkExecution mCurrExe;
    private Iterator<BenchmarkFile> mBenchmarkIterator;

    public BenchmarkListFragment() {
        super();
        // Get a list of all our parameters
        mBenchmarks = FileManager.getInstance().getBenchmarks();
        mNumSelected = 0;
    }

    public void startExecution() {
        // bring up the dialog
        showExecutionDialog();
        // wait for the dialog to show before continuing
        // start the first execution TODO make this work for multiple executions in the same file


    }

    public void showExecutionDialog() {
        // TODO disable the execute button, FAB
        // Create an instance of the dialog fragment and show it
        mDialog = new ExecutionDialog();
        mDialog.setExecutionDialogListener(this);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        mDialog.show(ft, "Hello");
    }

    @Override
    public void onActionButtonPressed() {
        // Find which benchmarks are selected and execute them
    }

    @Override
    public void onFragmentFocused() {
        // Make sure to change the toolbar back
        Toolbar toolbar = (Toolbar) mActivity.findViewById(R.id.toolbar);
        toolbar.setTitle("Benchmarks");
        mActivity.setButtonVisible(true);
        ((FloatingActionButton) mActivity.findViewById(R.id.fab)).show();

    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_benchmarklist;
    }

    @Override
    public void setupView(final View v) {
        mListView = (ListView) v.findViewById(R.id.list_benchmarks);
        final BenchmarkListAdapter listAdapter = new BenchmarkListAdapter(v.getContext(), mBenchmarks);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listAdapter.onItemClicked(position, view);
            }
        });
        mListView.setAdapter(new BenchmarkListAdapter(v.getContext(), mBenchmarks));
        mListView.setItemsCanFocus(false);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BenchmarkFile fileClicked = (BenchmarkFile) mListView.getAdapter().getItem(position);
                fileClicked.setSelected(!fileClicked.getSelected());
                if (fileClicked.getSelected()) {
                    incrementNumSelected();
                } else {
                    decrementNumSelected();
                }
                ((CheckBox) mListView.getChildAt(position - mListView.getFirstVisiblePosition()).findViewById(R.id.check_selected)).setChecked(fileClicked.getSelected());
            }
        });
        if (mActivity != null) {
            ((FloatingActionButton) mActivity.findViewById(R.id.fab)).show();
            Toolbar toolbar = (Toolbar) mActivity.findViewById(R.id.toolbar);
            toolbar.setTitle("Benchmarks");
        }
        mSwipeLayout = (SwipeRefreshLayout) v.findViewById(R.id.layout_listRefresh);
        mSwipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        BenchmarkListFragment.this.refreshList();
                    }
                });
            }
        });

    }

    public void clearListCheck() {
        for (int i = 0; i < mListView.getAdapter().getCount(); i++) {
            ((BenchmarkFile) mListView.getAdapter().getItem(i)).setSelected(false);
        }
        mNumSelected = 0;
    }

    public void refreshList(){
        if (mBenchmarks != null && mListView != null && mSwipeLayout != null) {
            mBenchmarks = FileManager.getInstance().getBenchmarks();
            mListView.setAdapter(new BenchmarkListAdapter(mListView.getContext(), mBenchmarks));
            mNumSelected = 0;
            onNoItemSelected();
            mSwipeLayout.setRefreshing(false);
        }
    }

    public void setBenchmarkListListener(BenchmarkListListener l) {
        this.mListListener = l;
    }

    private void incrementNumSelected() {
        mNumSelected++;
        if (mNumSelected == 1) {
            onFirstItemSelected();
        } else if (mNumSelected == 2) {
            onSecondItemSelected();
        }
    }

    private void decrementNumSelected() {
        mNumSelected--;
        if (mNumSelected == 0) {
            onNoItemSelected();
        } else if (mNumSelected == 1) {
            onFirstItemSelected();
        }
    }

    private void onFirstItemSelected() {
        //TODO
        if (mListListener != null) {
            mListListener.onFirstItemSelected();
        }
    }

    private void onSecondItemSelected() {
        // TODO
        if (mListListener != null) {
            mListListener.onSecondItemSelected();
        }
    }

    private void onNoItemSelected() {
        //TODO
        if (mListListener != null) {
            mListListener.onNoItemSelected();
        }
    }

    @Override
    public void println(final String output) {
        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDialog.println(output);
            }
        });

    }

    @Override
    public void print(final String output) {
        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDialog.print(output);
            }
        });

    }


    @Override
    public void onTaskBegin(BenchmarkExecution f, final int numIterations) {
        // TODO add as needed
        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDialog.setProgressMax(numIterations);
                if (mListListener != null) {
                    mListListener.onExecutionStart();
                }
            }
        });

    }

    @Override
    public void onTaskProgress(BenchmarkExecution f, final int iterationCompleted) {
        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDialog.setProgress(iterationCompleted);
            }
        });

    }

    @Override
    public void onTaskComplete(BenchmarkExecution f, String data) {
        mTaskInFile++;
        // see if this was the final task in the file. If so, write all data to the file and clear it
        if (mTaskInFile >= mCurrScript.getNumAttributes()) {
            // reset mTaskFile, write the data to a file, and execute the next one
            mData = mData + "\n" + data;
            mTaskInFile = 0;
            FileManager.getInstance().writeFile(mCurrScript.getFilename(), mData, false);
            mData = "";
            mDialog.invalidate();
            executeNextScript();
        } else {
            // otherwise, execute the next task in the script
            if (mTaskInFile == 1){
                mData = mData + data;
            } else{
                mData = mData + "\n" + data;
            }

            mDialog.invalidate();
            this.executeTaskInFile(mTaskInFile);
        }
        // if not, execute the next task in the file

    }

    @Override
    public void onTaskInterrupted(BenchmarkExecution f, int iterationsCompleted, String data) {
        mData = mData + data + "\n";
        FileManager.getInstance().writeFile(mCurrScript.getFilename() + ".i", mData, false);
        mDialog.invalidate();
        mDialog.onExecutionDone();
    }

    @Override
    public void onDialogShow() {
        // get the first selected script, this will start things off
        mBenchmarkIterator = mBenchmarks.iterator();
        executeNextScript();
    }

    private void executeTaskInFile(int index){
        mCurrExe.setBenchmarkActivityListener(null);
        mCurrExe.setBenchmarkPrinter(null);
        mCurrExe = BenchmarkManager.getInstance().find(mCurrScript.getAttribute(index).getBenchmarkName());
        mCurrExe.setBenchmarkActivityListener(this);
        mCurrExe.setBenchmarkPrinter(this);
        mCurrExe.executeTask(mCurrScript.getAttribute(index), (Context) this.getActivity());
    }

    private void executeNextScript() {
        mTaskInFile = 0;
        if (mBenchmarkIterator.hasNext()) {
            mCurrScript = mBenchmarkIterator.next();
            while (mBenchmarkIterator.hasNext() && !mCurrScript.getSelected()) {
                mCurrScript = mBenchmarkIterator.next();
            }
            if (mCurrScript.getSelected() && !mDialog.done) {
                mCurrExe = BenchmarkManager.getInstance().find(mCurrScript.getAttribute(0).getBenchmarkName());
                mCurrExe.setBenchmarkActivityListener(this);
                mCurrExe.setBenchmarkPrinter(this);
                mCurrExe.executeTask(mCurrScript.getAttribute(0), (Context) this.getActivity());


            } else {
                // tell the nice dialog we're done
                if (!mDialog.done) {
                    mDialog.onExecutionDone();
                }
            }
        } else {
            // tell the nice dialog we're done
            if (!mDialog.done) {
                mDialog.onExecutionDone();
            }
        }
    }

    @Override
    public void onDialogCancel() {
        if (mCurrExe != null) {
            mCurrExe.interruptTask();
        }
    }

    @Override
    public void onDialogClose() {
        // TODO re-enable the execute button and FAB
        if (mListListener != null) {
            mListListener.onExecutionEnd();
        }
    }

    public interface BenchmarkListListener {

        public void onFirstItemSelected();

        public void onSecondItemSelected();

        public void onNoItemSelected();

        public void onExecutionStart();

        public void onExecutionEnd();
    }
}
