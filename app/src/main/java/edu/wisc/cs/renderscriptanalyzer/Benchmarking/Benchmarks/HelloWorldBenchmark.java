package edu.wisc.cs.renderscriptanalyzer.Benchmarking.Benchmarks;

import edu.wisc.cs.renderscriptanalyzer.Benchmarking.BenchmarkExecution;
import edu.wisc.cs.renderscriptanalyzer.Scripting.EArgs;

/**
 * Created by Sam on 3/17/2016.
 * Benchmark to test the functionality of the application. Will not be used for gathering
 * meaningful data.
 */
public class HelloWorldBenchmark extends BenchmarkExecution {
    private int mIterations = 0;


    @Override
    protected void doIterationSetup(EArgs args) {
        //println("Setting up for RenderScript execution iteration");
    }

    @Override
    protected void doIterationWork(EArgs args) {
        //println("Hello, RenderScript world from iteration " + mIterations + "!");
        mIterations++;
    }

    @Override
    protected void doIterationCleanup(EArgs args) {
        //println("Cleaning up from RenderScript execution iteration");
    }

    @Override
    protected int getLayoutRes() {
        return 0;
    }

    @Override
    protected void doTotalSetup(EArgs args) {
        println("Benchmark Begin!");
        mIterations = 0;
        String extraMessage = args.getString("Extra Message", "Default extra message!");
        int extraInt = args.getInt("Extra int", 0);
        long extraLong = args.getLong("Extra long", 0L);
        double extraDouble = args.getDouble("Extra double", 0.0);
        boolean extraBoolean = args.getBoolean("Extra boolean", false);
        println("Extra message is " + extraMessage);
        println("Extra int is " + extraInt);
        println("Extra long is " + extraLong);
        println("Extra double is " + extraDouble);
        println("Extra boolean is " + extraBoolean);
    }

    @Override
    protected void doTotalCompletion(EArgs args) {
        println("Work done");
    }

    @Override
    protected void addProgramArguments(EArgs args) {
        args.addArgument("Extra message", "Default extra message!");
        args.addArgument("Extra int", 0);
        args.addArgument("Extra long", 0L);
        args.addArgument("Extra double", 0.0);
        args.addArgument("Extra boolean", false);
        mBenchmarkName = "Hello World Benchmark";
    }

    @Override
    public boolean isValidArgument(String argName, Object value) {
        return true;
    }

    @Override
    protected void getProgramArgs(EArgs args) {

    }
}
